*  let 变量 
    - 不能重复声明
    - 块级作用域
    - 不存在变量提升  变量声明之前使用会报错
    - 不影响作用域链
---
* const 常量
    - 声明后值不能进行更改
    - 一定要有初始值
    - 常量命名一般为大写 小写也不会报错
    - 块级作用域
    - 当使用const 定义对象或数组时 对其元素进行修改 是可以的，不会报错 因为这样不算对常量的修改
---
* 数组解构

    ```js
        const USER = ['春天','夏天','秋天','冬天']

        let [cun,xia,qiu,dong] = USER
    ```
---
* 对象解构
     ```js
        const USER = {
            name:'清风',
            age:15
        }

        let {name,age} = USER
    ```
---
* 模板字符串

    - 使用 `` 反引号进行定义 里边可以换行
    - 直接进行变量拼接 使用 ${} 
---
* 简化对象 
    ```js
        var name = 'haha'
        var age = 12

        //当对象中属性名和变量名相同 可以不用写  name:name
        var clas = {
            name,
            age
        }
    ```
---
* 箭头函数
    - 例： () => {}
    - this指向不会发生改变
    - 不能作为构造实例化对象   
    - 不能使用 arguments 变量
    - 当形参有且只有一个可以省略小括号 (item) =>{} ==  item => {}
    - 当代码体只有一条语句时可以省略花括号 如果带有return return也可以省略 (item) => {return item++}   ==  item => item++ 
---
* 函数默认值
    ```js
        function use (a,b,c=9){
            return a+b+c
        }
        //当 没有给其传值时，使用默认值
        use(1,2)

    ```
---
* rest参数
    ```js
        function qing(){
            //获取到的是一个是一个对象
            console.log(arguments)
        }
        function qing(...args){
            //使用 ...arg 的形式获取到的是一个数组 ['春','夏','秋']
            // ...arg永远是最后一个参数
            console.log(args)
        }
        qing('春','夏','秋')
    ```
---
* 扩展运算符
    - ... 将数组进行展开 
    ```js
        var s = ['春','夏','秋']

        //合并数组
        var y = [...s,'冬','天']
        // y = '春','夏','秋','冬','天'

        //2. 数组的克隆
        const sanzhihua = ['E','G','M'];
        // ['E','G','M']
        const sanyecao = [...sanzhihua];

        //将伪数组转化为数组
        const divs = document.querySelectorAll('div'); 
        const divArr = [...divs];

    ```
---
* symbol 数据类型  
    - 不能进行运算
    - 表示独一无二的值
    - 不能进行遍历
    - 不能强制转换
    ```js
        let name = Symbol()
        console.log(name,'---name')  // Symbol()  他是一个独一无二的值，但是我们不可见
        console.log(typeof name,'---typeof name')  // 类型是symbol
        let obj = {
            name:'mock',
            age:18,

        }

        // 使用这种形式可以在不清楚 obj 中到底存不存在 name 属性的时候使用
        // 它 不会 和 obj 内原本的 name 发生冲突
        obj[name] = 'jock' 

        console.log(obj[name],'---obj[name]')  // jock
        console.log(obj.name,'---obj.name')  //  mock

        console.log(Symbol() === Symbol(),'---Symbol() === Symbol()') //false  每一个Symbol都是独一无二的值  所以不会相等

        let age = Symbol('age') // 它是相当于一个标识符 输出obj的时候 可以知道Symbol代表的是什么意思
        obj[age] = 20


        console.log(obj[age],'---obj[age]')  //  20

        // false  age 只是一个标识符 并不会影响Symbol值的变化
        console.log(Symbol('age') == Symbol('age'),"---Symbol('age') == Symbol('age')") 

        // 使用.for的情况下是可以改变Symbol的唯一性的 故：两值相等 true
        console.log(Symbol.for('aa') == Symbol.for('aa'),"---Symbol.for('aa') == Symbol.for('aa')") 
        
        // 在创建的时候会检查全局是否寻在这个key的symbol.如果存在就直接返回这个symbol,如果不存在就会创建，并且在全局注册。
        let grand = Symbol.for('grand')
        let grand1 = Symbol.for('grand')

        obj[grand] = 'haha'
        obj[grand1] = 'xixi'

        // 因为使用Symbol.for后两个值是相等的所以最后一个会覆盖上一个的值
        console.log(obj[grand],'---obj[grand]') // xixi
        console.log(obj[grand1],'---obj[grand1]') // xixi

        // Symbol.keyFor 是获取相对应的key   
        console.log(Symbol.keyFor(grand1),'---Symbol.keyFor(grand1)') // grand
        // 📢： 只有使用Symbol.for()的形式才会有对应的key   
        console.log(Symbol.keyFor(age),'---Symbol.keyFor(age)') //  undefined

        // Symbol是禁止强制转换的  会报错
        // console.log(Symbol()+'')

        // Symbol是禁止运算的  会报错
        // console.log(Symbol()+1)

        // Object.getOwnPropertySymbols  用来获取使用 Symbol类型的key的
        // 例：  输出的就是  Symbol('hu') 不管obj中有多少使用Symbol返回的都是一个数组
        let hu = Symbol('hu')
        obj[hu] = '🐅'
        console.log(Object.getOwnPropertySymbols(obj),'---Object.getOwnPropertySymbols(obj)')

        console.log(obj,'---obj')

    ```
---

* 迭代器

    - ES6 创造了一种新的遍历命令 for...of 循环，Iterator 接口主要供 for...of 消费; 原生具备 iterator 接口的数据(可用 for of 遍历):
    - 创建一个指针对象，指向当前数据结构的起始位置;

    - 第一次调用对象的 next 方法，指针自动指向数据结构的第一个成员;
    - 接下来不断调用 next 方法，指针一直往后移动，直到指向最后一个成员; 4. 每调用 next 方法返回一个包含 value 和 done 属性的对象;
    - 注:需要自定义遍历数据的时候，要想到迭代器;

---

* 生成器
    - 是一个特殊函数
    - 用于进行异步编程
    - 例：
    ```js
        // 获取用户信息
        function user(){
            setTimeout(()=>{
                let ss = 0

                // 这个定时器就是相当于调用接口的存在
                let just = setInterval(()=>{

                    if(ss >= 4){
                        // 接口调用成功
                        let data = {id:0,name:'xiaoming',isxian:true}
                        // 用户id传递给getList用来获取列表数据
                        chen.next(data.id)
                        // 清除定时器
                        clearInterval(just)
                    }
                    ss++

                },1000)
                
            },1000)
        }

        // 根据用户id获取列表
        function getList(id){
            setTimeout(()=>{
                let ss = 0
                // 获取到了用户id  根据用户id获取列表
                console.log(id,'id')
                // 这个定时器就是相当于调用接口的存在
                let just = setInterval(()=>{
                    if(ss >= 4){
                        // 列表获取成功
                        let data = {id:3,name:'lihong',isxian:false}
                        
                        chen.next(data.isxian)
                        clearInterval(just)
                    }
                    ss++
                },1000)
                
            },1000)
        }
        function getPhone(xian){
            setTimeout(()=>{
                let ss = 0
                // 获取到了传递的值
                console.log(xian,'xian')
                // 这个定时器就是相当于调用接口的存在
                let just = setInterval(()=>{
                    if(ss >= 4){
                        // 接口获取成功
                        clearInterval(just)
                        let data = {id:0,name:'xiaoming',isxian:true}
                    }
                    ss++
                },1000)
                
            },1000)
        }

        function * onLoad(){
            // 在user方法内调用  chen.next(id) 并传值  在这里进行接收
            let id = yield user()
            // 在getList方法内调用  chen.next(xian) 并传值  在这里进行接收
            let xian = yield getList(id)
            // 将值传递给 getPhone方法
            yield getPhone(xian)
        }
        let chen = onLoad()
        chen.next()
    ```
---

* promise
    - 异步调用
    ```js
        // 定义一个方法返回promise对象
        function cunMu(){
            // resolve 数据正常调用      reject 数据错误调用
            return new Promise((resolve,reject)=>{
                let isXian = true

                if(isXian){
                    // 返回正常的数据
                    return resolve('对的')
                }
                // 返回错误数据
                reject('错的')
            })
        }

        // 定义一个promise对象
        // resolve 数据正常调用      reject 数据错误调用
        // let just = new Promise((resolve,reject)=>{
        //     let isXian = true

        //     if(isXian){
        //         return resolve('对的')
        //     }
        //     reject('错的')
        // })

        cunMu().then(value => {
            // 介绍正常数据
            // 返回一个promise数据
            return new Promise((resolve,reject)=>{
                reject('haha')
            })
        },reason => {
            // 接收错误数据
            console.log(reason)
        }).then(value=>{
            // 他因为返回的是一个promise所以他还有.then方法
            console.log(value)
        },reason => {
            console.log(reason)
        })
    ```
---
* set集合 
    ```js
        //定义一个set
        var just = new set(['app'])

        just.size  //获取set的元素个数
        just.add('qing')   // 向set中添加新的元素
        just.delete('app') //删除元素
        just.has('qing')   // 检查set的元素中是否有 qing 有返回 true 无 返回false
        just.clear() //清空set的所有元素
        // 可以使用 for ...of 进行遍历 
        let just = [1,2,3,2,3,1,4,5,3,1,9]
        // console.log(just)

        // 数组去重 使用 ...new Set()
        let arr = [...new Set(just)]
        // console.log(arr)

        // 交集
        let just1 = [3,4,9,3,7,0,9]

        // 先将just去重  [...new Set(just)] new Set返回的不是一个数组 所以不能使用.filter方法
        // 使用filter方法创建一个新的数组，新数组中的元素是通过检查指定数组中符合条件的所有元素。
        // 使用has方法判断是否存在  存在就将其存入新数组中
        
        let epson = [...new Set(just)].filter(item => new Set(just1).has(item))

        // 合集
        // 将just和just1进行合并并去重
        let newArr = [...new Set([...just,...just1])]
    ```

---
* map集合

---
* class
    ```js

        // 创建一个class类
        class just{
            // 构造函数  name和age都是实例化时传递的参数
            constructor(name,age){
                this.name = name
                this.age = age
            }

            // 方法存在于原型上不在实例身上
            feng(){
                console.log('my name is '+ this.name)
            }
        }
        let epson = new just('zs',28)

        epson.feng()

        console.log(epson)

        // 继承just类  extends 属于继承的标识符
        class yuan extends just {
            // 构造函数
            constructor(name,age,cart,sex){
                // 继承的类的构造函数
                super(name,age)
                this.cart = cart
                this.sex = sex
            }

            hou(){
                console.log('houhouoh')
            }
            huai(){
                console.log('huaihuaihuai')
            }

            // get 监听读取
            get quan(){
                console.log('读取触发')
                // 他是有一个返回值的  输出读取的时候会返回出去
                return '莫西莫西'
            }
            // set 监听修改  必须有参数 输出赋值时默认返回更改的值
            set quan(newVal){
                console.log('修改触发')
            }
        }

        let ping = new yuan('ls',18,'奥迪','男')

        ping.feng()
        ping.hou()
        console.log(ping)
        console.log(ping.quan)
        console.log(ping.quan = 11)
    ```
---
* 数值扩展
    ```js
        console.log(0.2+0.1 === 0.3)

        // Number.EPSILON  表示最小精度
        // 两个的值相差 小于 最小精度  的算就算两个值相等
        console.log((0.2+0.1) - 0.3 < Number.EPSILON)

        // Number.isFinite() 检查一个数值是否有限
        console.log(Number.isFinite(2/3))

        // Number.isNaN() 判断一个值是否为NaN
        console.log(Number.isNaN())

        // Number.parseInt 转换为数字
        console.log(Number.parseInt('12013t3'))

        // Number.parseFloat 转换为浮点数
        console.log(Number.parseFloat('1.39284'))

        // Math.trunc  去除一个数的小数部分返回整数  不存在四舍五入 小数点后都会舍去
        console.log(Math.trunc(9.8))

        // Number.isInteger  判断一个数值是否为整数 返回 true 或 false
        console.log(Number.isInteger(1.2))
    ```
---
* 对象扩展
    ```js
        // Object.is() 判断两个值是否相等 相当于 === 但是 -0 NaN除外

        console.log(Object.is(-0,0)) // false
        console.log(-0 === 0) // true
        console.log(Object.is(1,1)) // true
        console.log(Object.is(NaN,NaN)) // true
        console.log(NaN === NaN) // false

        // Object.assign()  用来合并对象  
        let just = {
            name:'cc',
            age:1,
        }
        let just1 = {
            sex:'女',
            name:'fzc'
        }

        // 📢： 当属性名一样时永远以第二个参数的为主
        let newJust = Object.assign(just,just1)
        console.log(newJust)
    ```
---
# es7
* includes
    - 判断数组中是否存在某个元素 和 indexOf 相同 不过返回值为true或false
    ```js
        let just = ['haha','heihei','hehe','xixi']
        console.log(just.includes('haha')) // true
        console.log(just.includes('haha2')) // false
    ```
---
* 对象拓展
    - Object.keys(对象)  获取所有键
    - Object.values(对象) 获取所有值
    - Object.entries(对象) 将键与值转成一个数组
    - Object.getOwnPropertyDescriptors  对象属性的描述方法  用法： 1.配合Object.create来克隆对象  2.将源对象的所有描述对象提取出来
    ```js
        // 获取所有的键
        let just = {
            name:'cc',
            age:1,
        }
        console.log(Object.keys(just),'Object.keys')
        // 获取所有值
        console.log(Object.values(just),'Object.values')
        // 将源对象的所有描述对象提取出来
        console.log(Object.getOwnPropertyDescriptors(just),'Object.getOwnPropertyDescriptors')

    ```
    ```js
        // 扩展运算符可以扩展对象
        let just = {
            name:'cc',
            age:1,
        }
        console.log(...just) // 打印： name:'cc',age:1
    ```
---
* 正则扩展
    - 
    ```js
         let str = '<a href="http://www.baidu.com">百度</a>'

        // 不使用新命名捕获 数据只能通过下标进行获取，后期维护不方便 
        let reg = /<a href="(.*)">(.*)<\/a>/
        let newStr = reg.exec(str)
        console.log(newStr)

        // 使用了捕获 数据可以通过自己设置的键来获取对应的值 在 groups 下
        let reg2 = /<a href="(?<url>.*)">(?<desc>.*)<\/a>/
        let newStr2 = reg2.exec(str)
        console.log(newStr2)

        <!-- 正则断言 -->

         let just = '2422rwfa是你发134两件事'
        // 获取 两 前边的数字    正向断言
        let qing = /\d+(?=两)/
        // 获取发后边的数字    反向断言
        let qing2 = /(?<=发)\d+/
        console.log(qing.exec(just))
        console.log(qing2.exec(just))


        <!-- dotall模式 -->

         // 声明正则匹配a中的数据和span中的数据 
        // . 可以匹配除换行符以外的任意单个字符
        let reg = /<li>\s+<a href="#">(?<zhong>.*?)<\/a>\s+<span>(?<ying>.*?)<\/span>/
        let just = reg.exec(str)
        console.log(just.groups)

        // 正则的后边加上 /s 后 . 可以匹配任何字符
        let reg2 = /<li>.*?<a href="#">(?<zhong>.*?)<\/a>.*?<span>(?<ying>.*?)<\/span>/gs
        let just2;
        let feng = []
        while (just2 = reg2.exec(str)) {
            feng.push(just2.groups)
        }
        console.log(feng)
    ```
---
* es10 对象扩展
    -   
    ```js
         // 将 二维数组 转化为 对象 
        let qing = Object.fromEntries([
            ['name','ls'],
            ['just','you good,ask one down,you have girl friend']
        ])
        console.log(qing,'将 二维数组 转化为 对象 ')

        // 将 对象 转为 二维数组
        let feng = Object.entries(qing)
        console.log(feng,'将 对象 转为 二维数组')
    ```
---
* es10 字符串扩展
    - 
    - trimStart() 清除左侧空格
    - trimEnd()  清除右侧空格
    ```js
        let str = '   just Happy   '
        console.log(str)
        console.log(str.trim(),'清除两侧空格')
        console.log(str.trimStart(),'清除左侧空格')
        console.log(str.trimEnd(),'清除右侧空格')
    ```
---
* es10 数组扩展
    - 
    - flat   将多维数组转换为低维数组
    - flatMap  相当于 flat() 和 map() 的一个集合 📢 ：只能转换二维数组
    ```js
         let qing = [1,2,3,[2,3,5]]
        console.log(qing.flat(),'将多维数组转换为低维数组')
        let ming = [1,2,3,4,[3,4,5,6,[2,4,6,8]]]
        console.log(ming.flat(),'flat()的默认值为 1 只会往下找一层')
        console.log(ming.flat(2),'设置flat()的值为 2 让其向下寻找两层')

        let shang = [1,2,3,4,5,6,7]
        // 当返回的是一个复杂结构的时候可以使用 flatMap 来将其转换为一维数组
        // flatMap 相当于 flat() 和 map() 的一个集合 📢 ：只能转换二维数组
        let he = shang.flatMap(item => [item * 5])
        console.log(he)
    ```
---
* 私有属性
    - 
    - 使用 #定义的属性都是私有属性  私有属性只能在内部使用不能在外部使用
    ```js
        class Just{
            // 公有属性
            name;
            // 私有属性
            #age;
            #work;
            constructor(name,age,work){
                this.name = name
                this.#age = age
                this.#work = work
            }
            yuan(){
                console.log(this.#age)
            }
        }
        let qing = new Just('ls',20,'外卖')

        console.log(qing)
        // 私有属性只能在类内部进行使用
        qing.yuan()

        // console.log(qing.#age) // 私有属性不能在外部调用 会报错
    ```
---
* promise的 allSettled 方法和 all方法
    - 
    ```js
         // 声明两个promise对象
        let qing = new Promise((resolve,reject)=>{
            resolve('成功的！')
        })
        let feng = new Promise((resolve,reject)=>{
            resolve('同样成功的！')
            // reject('你错了！')
        })

        // 可以获取两个promise对象是否成功的状态
        // 两个promise对象有一个返回成功 结果就为成功
        let just = Promise.allSettled([qing,feng])

        // 不能获取是否成功的状态 只能获取返回的结果
        // 两个promise对象有一个返回失败 结果就为失败
        let just1 = Promise.all([qing,feng])
        console.log(just)
        console.log(just1)
    ```
---
* matchAll 提取正则匹配到的全部数据
    - 
    ```js
        let reg2 = /<li>.*?<a href="#">(?<zhong>.*?)<\/a>.*?<span>(?<ying>.*?)<\/span>/gs

        let str = `
        <ul>
            <li>
                <a href="#">你好</a>
                <span>you good</span>
            </li>
            <li>
                <a href="#">你们好</a>
                <span>yous good</span>
            </li>
        </ul>
        `
        // 将所有正则匹配到的数据都存在一起 比1.0的方法更简单便捷
        let feng2 = str.matchAll(reg2)
        for (const val of feng2) {
            console.log(val,'val')
        }
        // 📢 ：直接输出feng2是看不到数据的
        console.log(feng2,'feng2')
        // 可以使用展开运算符帮助查看数据
        console.log(...feng2,'feng2')
    ```
* 可选链操作符
    - 
    ```js
        let just = {
            ageList : {
                name:'ls'
            }
        }
        // 之前输出just中ageList下的name
        let name = jus ? jus.ageList ? jus.ageList.  name : '' : ''

        // 使用链操作符  简洁更容易懂 特别是多层的时候优势更大
        let name2 = jus?.ageList?.name

        console.log(name2)
        console.log(name)

    ```
---
* import动态引入
    - 
    ```js
        // 默认引入js
        import just from './just'
        // 新型引入js
        import('./just').then(val => {
            // val 是  just.js里抛出的对象
            console.log(val)
        })
    ```
* BigInt 类型
    - 
    - bigInt类型 不能和非 bigInt类型 的数进行运算
    - 浮点型不能转成 bigInt类型 

    ```js
        // bigInt类型 不能和非 bigInt类型 的数进行运算
        // 浮点型不能转成 bigInt类型 
        let just = 123n // 数据后跟n表示 bigInt类型 
        console.log(just,typeof just)

        let qing = 123
        // 使用 BigInt方法将普通类型转换为bigInt
        console.log(BigInt(qing))

        // 最大值
        let max = Number.MAX_SAFE_INTEGER
        console.log(max) // 9007199254740991
        console.log(max + 1) // 9007199254740992
        console.log(max + 2) // 9007199254740992 就不能再出现比其最大的值

        console.log(BigInt(max))  // 9007199254740991n
        console.log(BigInt(max) + 1n) // 9007199254740992n
        console.log(BigInt(max) + 2n) // 9007199254740993n
        console.log(BigInt(max) + 3n) // 9007199254740994n //可以突破最大值
    ```
---
# 全局this  -- globalThis --
```js
    // 不管在哪个位置 globalThis 输出始终指向 window 永远不会发生改变
    class mack{
        constructor(name,age){
            this.name = name
            this.age = age
        }
        qing(){
            console.log(this,'指向mack') // 指向mack 
            console.log(globalThis,'依旧指向window') // 依旧指向window
        }
    }
    let mockJs = new mack('ls',18)
    mockJs.qing()
```