# 1.包的初始化

```shell
npm init -y  #这里文件夹名字不能有 中文 空格 纯数字
```

# 2.安装包

```shell
npm i 包名
npm install 包名
npm i  包名@版本号  => npm i jquery@2.22.2
```

# 3.安装全局包

```shell
npm i 包名 -g
```

# 4.卸载包

```shell
npm uninstall 包名
npm uninstall 包名@版本号
npm uninstall 包名 -g # 卸载全局包
```

# 5.nrm的使用

```shell
1. npm i nrm -g # 安装nrm
2. nrm ls # 展示可用的镜像链接
3. nrm use 镜像名字
```

# 6.包的发布

```shell
1. nrm use npm #切换为官方镜像
2. npm login #登陆输入账号和密码
3. npm  publish #注意终端要在项目的根目录
4. npm  unpublish 包名 --force #删除已发布的包
```

# 7. 安装nodemo的使用

```shell
npm i nodemon -g
```

