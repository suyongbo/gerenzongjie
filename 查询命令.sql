-- 查询所有数据
SELECT * FROM `team`;
-- 查询单个字段的数据
select uname from team;
-- 添加数据
-- insert into team(uname,age,sex) values ('张菲',18,'女');
-- 修改id为4的年龄为22
update team set age=22 where id = 4;
-- 修改id为3的年龄和性别  修改多个中间用','分割
UPDATE team set age=23,sex='女' where id=3;
-- 删除id为3的那一行
DELETE from team where id=3;
-- 查询年龄为22的数据
select * from team WHERE age = 22;
-- 查询年龄不等于22的数据
select * from team where age !=22;
-- 查询年龄为18岁以上的女性
select * from team where age>18 and sex='女';
-- 查询性别是男的或者年龄小于等于18岁的数据
select * from team where sex='男' or age = 18;
-- 按照年龄进行升序排列  order by默认为升序  asc也代表升序
select * from team order by age;
select * from team order by age asc;
-- 按照年龄进行降序排列
select * from team order by age desc;
-- 先按照年龄进行升序排列，再根据名字的首字母进行降序排列
select * from team order by age , uname desc;
-- 获取表格中的总条数 并使用as进行设置别名
select count(*) as '总条数' from team;