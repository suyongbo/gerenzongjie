### 禁用按钮使用disabled

# 面向对象

### 面向过程编程

面向过程：就是分析出解决问题所需要的步骤，然后用函数把这些步骤一步一步实现，使用的时候再一个一个的依次调用

### 面向对象编程OOP

面向对象：就是把事务分解成一个个对象，然后由对象之间分工与合作

##### 面向对象的特性

- 封装性
- 继承性
- 多态性

### 面向过程和面向对象的对比

![image-20210424181453681](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210424181453681.png)

### 类和对象

1. 对象

   对象由属性和方法组成

   ​	属性：事物的特征，在对象中用属性表示

   ​	方法：事物的行为，在对象中用方法表示

2. 类

   类抽象了对象的公共部分，泛指某一大类

   对象特指某一个，通过类实例化一个具体的对象

3. 创建类

   ```js
   class 类名 {
       
   }
   实例化这个类
   var 名 = new 类名();
   ```

4. 构造函数constructor

   constructor()方法是类的构造函数，用来传递参数，返回实例对象，通过new命令生成对象实例时，自动调用方法，如果没有定义，类内部会自动创建一个constructor()

5.   添加方法  

   ```js
   多个方法不用使用逗号分割
   Sing(call) {
       console.log(call);
   }
   ```

### 类的继承

```js
class Num {
    
    constructor() {
        
    }
    // 添加方法   多个方法不用使用逗号分割
    Sing(call) {
        console.log(call);
    }
}
使用son继承num的所有内容
class Son extends Num {}
// 就近原则，如果son里有Sing()这个方法就输出son里的
// 如果没有就找他的继承类里边有才输出
调用son就能直接使用继承过来的类
var inhe = new Son();
inhe.Sing('我能直接继承');
```

super关键字用于访问和调用对象父类上的函数，可以调用父类的构造函数，也可以调用父类的普通函数

```js
class Father {
zi() {
        return '你好';
    }
}
//   使用son继承num的所有内容
class Son extends Father 
    zi() {
        // 使用super调用父类的普通方法
        console.log(super.zi() + '调用者');
    }
 }

//   调用son就能直接使用继承过来的类
var inhe = new Son();
inhe.zi();
```

### 双击事件

ondblclick鼠标双击触发

  配合双击禁止选中文字使用

  window.getSelection

   ? window.getSelection().removeAllRanges()

   : document.selection.empty();

![image-20210427093116847](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210427093116847.png)

### 新的添加标签方法insertAdjacentHTML

```js
直接写一个字符串
var tj = `<li class="liactive"><span>测试1</span><span class="iconfont icon-guanbi"></span></li>`;
将字符串添加到ul的最后一个子元素
run.ul.insertAdjacentHTML('beforeend', tj);
```

### 实例成员和静态成员的区别

![image-20210427143322260](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210427143322260.png)

### 原型对象prototype是构造函数中的

![image-20210427144906149](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210427144906149.png)

```js
function Tu(uming, age) {
    this.uming = uming;
    this.age = age;
}
Tu.prototype.sing =function(){
          console.log('你好');
}
var su = Tu();
//可以使用他来调用原型对象prototype
su.sing();
```

公共的对象放到原型对象身上，能够节省内存

### 对象原型

```js
__proto__  是实例对象里的
```

对象原型和原型对象是相等的

![image-20210427153921393](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210427153921393.png)

### 原型链(不懂的重点)

![image-20210427163134706](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210427163134706.png)

### js 的查找机制

![image-20210428152055308](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210428152055308.png)

### 继承

可以通过构造函数+原型对象来模拟继承被称为组合继承

call方法

```js
function fn(x,y){
    console.log('你好')
    console.log(this)
    console.log(x,y)

}
var o = {
    name:'苏'
}
fn()
//call可以调用函数
fn.call()
//call()可以改变this指向  现在this指向o  call()可以传递参数
//第一个必须是this指向  如果this指向不变可以这样写
//fn.call(this,1,2)
fn.call(o,1,2)
```

使用call()进行继承属性

```js
//父构造函数
function fn(x,y){
    //this指向父构造函数的对象实例
    //fns构造函数中使用call()后  this==ni
    this.x = x
    this.y = y
}
//子构造函数
function fns(x,y){
    //this指向子构造函数的实例对象   this==ni
    //将父构造函数的this指向子构造函数的this
    fn.call(this,x,y)

}
var ni = new fns();

```

使用call()进行继承方法

```js
//父构造函数
function fn(x,y){
    //this指向父构造函数的对象实例
    //fns构造函数中使用call()后  this==ni
    this.x = x
    this.y = y
}
//父构造函数的原型对象
fn.prototype.money = function(){
    console.log(100)
}
//子构造函数
function fns(x,y){
    //this指向子构造函数的实例对象   this==ni
    //将父构造函数的this指向子构造函数的this
    fn.call(this,x,y)
}
//子原型对象等于父亲的实例对象    详情见下图
fns.prototype = new fn();
fn.constructor

var ni = new fns();

```

![image-20210429105226095](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210429105226095.png)

### 类

类的本质就是一个函数    类就是构造函数的另一种写法

![image-20210429112745396](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210429112745396.png)



### ES5中新增方法

1. 数组方法

   迭代方法：forEach()、map()、filter()、some()、every()

   forEach()的语法：相当于增强的for循环   和map()相似

   ```js
   数组.forEach(function(数组元素，索引号，数组本身){
       
   })
   
   var array = [1, 5, 8, 7, 24];
   array.forEach(function (value, index, arr) {
       console.log(`数组元素${value}`);
       console.log(`数组索引${index}`);
       console.log(`数组本身${arr}`);
   });
   ```

   filter()的语法  ：

   创建一个新数组，新数组中的元素是通过检查指定数组中符合条件的所有元素

   主要用于筛选数组         也可以进行循环     返回的是一个新数组

   ```js
   
   var a = 数组.filter(function(数组元素，索引号，数组本身){
       return 判断条件
   })
   
   
   var array = [1, 5, 8, 7, 24];
   var a = array.filter(function (value, index, arr) {
    //返回大于等于8的数   
       return value>=8;
       //返回数组中是偶数的
       //return value % 2 ===0;
   });
   返回的是一个数组，数组内容是大于等于8的
       console.log(a);
   ```

   some()语法：  和every()相似

   用于检测数组中的元素是否满足指定条件

   返回的是布尔值，查到就返回true，查不到就返回false，找到第一个满足条件的元素就不在查找

   ```js
   var a = 数组.some(function(数组元素，索引号，数组本身){
       return 判断条件
   }
   
   var array = [1, 5, 8, 7, 24];
   var a = array.some(function (value, index, arr) {
    //是否有大于等于8的数  有返回true  没有返回false
       return value>=8;
   });
   	返回的是一个布尔
       console.log(a);
   ```

### trim方法去除字符串两侧空格

```js
语法：
var x = '  a  '
console.log(x.trim())
```

### 对象方法

Object.defineProperty()定义新属性或修改原有的属性

![image-20210429153518693](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210429153518693.png)

# 函数进阶

new function ('参数1','参数2','函数体')

### 函数中this指向

![image-20210429162121048](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210429162121048.png)

### 改变this指向  有三种   1. call()  2. apply()  3. bind()

1. call()方法    主要用于继承   更详细的见上边  继承  的书写内容

   ```js
   function fn(x,y){
       console.log('你好')
       console.log(this)
       console.log(x,y)
   
   }
   var o = {
       name:'苏'
   }
   fn()
   //call可以立即调用函数
   fn.call()
   //call()可以改变this指向  现在this指向o  
   //call()可以传递参数
   //第一个必须是this指向  如果this指向不变可以这样写
   //fn.call(this,1,2)
   fn.call(o,1,2)
   ```

2. apply()方法  可以调用函数    主要应用 数学公式的计算

   ```js
   var o= {
       name:'你好'
   }
   function fn(arr){
       console.log(this)
       console.log(arr)//接受的是 哈喽 而不是数组
   }
   //第一个必须是this指向  
   //数组中有几个数组元素函数就要有几个形参
   fn.apply(o,['哈喽'])
   //apply()可以调用函数   也可以改变this指向
   //参数必须是数组(伪数组)
   //apply主要应用   可以借用数学内置对象求最大值
   例：
   var arr = [1,5,86,7]
   //把this指向Math调用对象
   var math = Math.max.apply(Math,arr)
   console.log(math)
   ```

3. bind()  不会调用函数   返回的是原函数改变this后的新函数

   ```js
   var o= {
       name:'你好'
   }
   function fn(a,b){
       console.log(this)
       console.log(a+b)
   }
   //不会调用函数  只会改变原来函数this指向   
   //有返回值   返回的是原函数改变this后的新函数
   //第一个必须是this指向  
   var f = fn.bind(o,1,2);
   f();
   ```

### call   apply   bind   总结

相同点：都可以改变函数内部的this指向

不同点：

 	1. call和apply会调用函数，并改变函数内部this指向
 	2. call和apply传递的参数不一样，call传递参数aru1，aru2形式apply则必须数组形式[arg]
 	3. bind 不会调用函数，可以改变函数内部的this指向

主要应用场景：

1. call经常做继承的时候使用
2. apply经常跟数组有关系，如：借助与数学对象实现数组最大值最小值
3. bind  不想调用函数，但还想改变this指向使用它，如：改变定时器内部的this指向

### 严格模式

![image-20210429182652256](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210429182652256.png)

### 开启严格模式

严格模式可以应用到整个脚本或个别函数，将其分为两种

1. 为脚本(script标签)开启严格模式

   在所有脚本(script标签)之前添加'use strict'

   ```js
   <script>
       'use strict';
   //只要在最上方添加了这样一段话，整个标签内都开启了严格模式
   //必须加引号
   </script>
   ```

2. 为函数开启严格模式

   在函数内部第一行添加'use strict';

   ```js
   <script>
   	function fn(){
            'use strict';
       //只作用与这个函数内函数外不影响
       }
   </script>
   ```

### 严格模式的变化

1. 变量规定

   - 变量必须先声明再使用

   - 严禁删除已经声明的变量

2. this指向

   - 全局作用域中函数中this的指向为undefined
   - 函数不加new调用，this会报错
   - 定时器this指向不变
   - 事件，对象还是指向调用者

3. 函数变化

   - 函数中的参数不能有重名
   - 不允许在非函数的代码块内声明函数  详细见下图：

   ![image-20210429184906979](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210429184906979.png)

### 高阶函数就是回调函数

将函数作为参数传递给另一个函数就是高阶函数

```js
function fn(a,b,callback){
    console.log(a+b);
    callback && callback();
}
fn(1,2,function(){
    console.log('现在我就属于高阶函数')
})
```

### 闭包

闭包指有权访问另一个函数作用域中变量的函数      闭包就是一个高阶函数

闭包的作用：延伸了变量的作用范围

简单理解，一个作用域可以访问另一个函数内部的局部变量  代码演示见下面

```js
//fnn能够访问fn中的局部变量，fn就是闭包   
function fn(){
    var num = 1;
    function fnn(){
        console.log(num)//输出1
    }
    fnn();
}
fn();
```

### 递归函数

函数内部自己调用自己，这个函数就叫递归函数    需要加上退出条件return

```js
//递归函数的作用和循环效果一样
var num = 1;
function fn(){
    console.log('打印六句话')
    if(num == 6){
        //由于递归很容易发生死循环，需要加上退出条件return
        return;
    }
    num++
    fn()
}
fn()
```

### 正则表达式

![image-20210430163334268](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210430163334268.png)

特点：

![image-20210430163451420](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210430163451420.png)

### 正则表达式的组成

1. 边界符

   ^  代表以符号后的内容开始

   $ 代表以符号前的内容结尾

2. 字符类  多选一

   [ ] 表示有一系列字符可供选择 只要匹配一个就可以  例：

   [abc] 只要有这三个的任何一个字母都返回true

   

   [-]  -表示范围  例：

   [ a-z ] 26个小写英文字母都返回true

   [a-zA-Z0-9]  只要包含范围内的任何一个就可以

   

   如果[]中有^表示取反，不能包含里边的内容   例：

   ​	[ ^a-zA-Z0-9]  不能包含包含范围内的任何一个

3. 量词符

   ![image-20210502101248534](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210502101248534.png)

4. 预定义类

   ![image-20210502111520039](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210502111520039.png)

   在正则表达式中‘|’表示  或者

5. 替换    字符串.replace()返回一个新字符

   ```js
   var tex = document.querySelector('textarea')
   var button = document.querySelector('button')
   var div = document.querySelector('div')
   
   button.addEventListener('click', function(){
       //字符串.replace('需要替换的字','替换成的字')。
       //其中g表示全局查询改变
       div.innerHTML =tex.value.replace(/[激情爱死傻屌杀]/g,'哈')
   })
   ```

### let关键字定义的是变量

1. 使用let关键字声明的变量不存在变量提升，只能先声明后使用
2. 防止循环变量变成全局变量
3. 使用let关键字声明的变量有暂时性死区特性

### const关键字定义的是常量

1. const定义的变量值不能改，不存在变量提升
2. 定义的时候要给初始值

### let、const、var的区别

![image-20210502152304163](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210502152304163.png)

### 数组解构

let [变量名] = 数组

```js
let err = [1,2,3,4];
let [a,b,c,d] = err
console.log(a)//a就是err中的元素1
```

### 对象解构

let {原对象中的属性名:自定义属性名} = 对象

```js
let ert = {
    name: '李磊',
    age: 18,
    sex: '男',
};
//对象名可改可不改
let { name: qq, age: nianling, sex } = ert;
console.log(qq);
console.log(nianling);
console.log(sex);
```

### 箭头函数

```js
//正常的
let fn = function(a,b){
    return a+b;
}
//箭头函数  
let fn = (a,b)=>{
    return a+b;
}
1.当箭头函数只有一行代码时可以省略{}和return
let fn = (a,b)=> a+b;
2.当箭头函数只有一个参数时可以省略()
let fn = a=>{
    return a+10;
}
也可以这样写
let fn = a => a+10;
箭头函数没有this 如果在箭头函数中使用this 那么this指向他定义当前位置的this

```

### 剩余参数

```js
one代表10   ...dou代表一个数组代表接受剩余的所有实参
const sum = (one,...dou) => {
    
};
sum(10,20,30,40)
```

### 扩展运算符

扩展运算符可以将数组或者对象转为都好分割的参数序列

```js
let ary = [1,2,3]
语法：...数组名
console.log(...ary)//输出1,2,3,

扩展运算符可以用来应用数组合并
let ary1 = [1,2,3]
let ary2 = [5,6,7]

方法1.
let arr = [...ary1,...ary2] 

方法2.
//将ary2作为元素添加到ary1中
ary1.push(...ary2)

console.log(ary1)//输出1,2,3,5,6,7
console.log(arr)//输出1,2,3,5,6,7


扩展运算符可以将伪数组转换成真正的数组

将伪数组使用...扩展运算符转换后放在真正的数组中
var s = [...伪数组名]
```

Array的扩展方法

![image-20210502163952895](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210502163952895.png)

![image-20210502164053368](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210502164053368.png)

![image-20210502164531357](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210502164531357.png)

![image-20210502195004764](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210502195004764.png)

![image-20210502195017972](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210502195017972.png)

### findIndex()

```js
let ary = [1,5,10,15]
let index = ary.findIndex((val,index) => val > 9)
返回第一个符合条件的索引
```

### String的扩展方法

startswidth()表示参数字符串是否在原字符串的头部 

endswidth()表示参数字符串是否在原字符串的尾部

![image-20210503092843835](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210503092843835.png)

repeat()将原字符串重复n次，返回一个新字符

![image-20210503093716668](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210503093716668.png)

### set数据结构

类似于数组，但成员的值是唯一的，没有重复的值   经常用来数组的去重

set是一个构造函数，用来生成set数据结构

```js
const s = new Set();
console.log(s.size)//返回set的值的个数(去重后的个数)   返回0
const s = new Set(['n','d']);
console.log(s.size)//返回2
```

### set中常用的方法

![image-20210503094723574](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210503094723574.png)







