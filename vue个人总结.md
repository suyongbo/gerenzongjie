# vue的使用

```js
//1.导入vue.js文件
<script src="./vue.js"></script>
//2.创建vue对象
<script>
    var ti = new Vue({
        //写入数据的位置
        el: '#chu',
        //data里存的是对象为显示的数据
        data: {
          pika: '你好吗？',
          nihao: '皮卡丘~？',
          hl: '<h1>hallo</h1>',
        },
    });
</script>
//3.给html标签赋值   数据以{{}}，双花括号的形式书写
//{{}}中可以进行简单的运算和字符拼接 注意：加号的前后需要跟空格进行分割
//例如：{{1 + 2}}  {{你好 + hallo}}

<div id="chu">{{pika}}</div>
```

# 指令

```html
1.v-colak

<style>
    /* 
    添加[v-colak]方法增强用户体验
    防止刷新页面时出现插值表达式的闪动
    */
    [v-colak] {
        display: none;
    }
</style>
<div v-colak>{{pika}} {{nihao}}</div>

2.v-text

<!-- 
v-text指令用于填充纯文本
不会出现差值闪动问题 
-->
<div v-text="pika"></div>

3.v-html

<!-- 
v-html指令用于填充html片段
但是有安全问题
-->
<div v-html="hl"></div>

4.v-pre

<!-- 
v-pre指令用于填充原始信息
直接输出{{pika}}不会进行编译
-->
<div v-pre>{{pika}}</div>

5.v-once

<!-- 
v-once指令  当前这条数据不再修改时使用
可以提高性能，减轻响应压力
-->
<div v-once>{{nihao}}</div>

6.v-model 

<!-- 
多用于input框
修改input框中的内容会使div中的数据跟着改变，
这就是双向数据绑定
-->
<div>{{dui}}</div>
<input type="text" v-model="dui" />
```

# 事件绑定

```html
<!-- 点击事件的绑定 -->
<button v-on:click="fu++">大</button>
<!-- 简便的点击事件绑定 -->
<button @click="fu--">小</button>
<!-- 调用方法进行事件绑定 -->
<button @click="shan">变大</button>
<!-- 另一种调用方法形式进行事件绑定 -->
<button @click="quan()">变小</button>

<script>
    const ha = new Vue({
        el: '#nong',
        data: {
            fu: 0,
        },
        // methods 里边写各种方法
        methods: {
            shan: function () {
                console.log(this == ha);
                // 结果为true表示this代表ha
                this.fu++;
            },
            quan: function () {
                this.fu--;
            },
        },
    });
</script>
```

# 事件修饰符

```html
阻止冒泡  在事件绑定后边添加.stop修饰符
<button v-on:click.stop="nihao">点击</button>
阻止默认行为   在事件绑定后边添加.prevent修饰符
<a v-on:click.prevent="nihao">点击</a>
```

# 按键修饰符

```html
<!-- 只有在 `keyCode` 是 13 时调用 `vm.submit()` -->
<input v-on:keyup.13="submit">

<!-- -当点击enter 时调用 `vm.submit()` -->
<input v-on:keyup.enter="submit">

<!--当点击enter或者space时  时调用 `vm.alertMe()`   -->
<input type="text" v-on:keyup.enter.space="alertMe" >

常用的按键修饰符
.enter =>    enter键
.tab => tab键
.delete (捕获“删除”和“退格”按键) =>  删除键
.esc => 取消键
.space =>  空格键
.up =>  上
.down =>  下
.left =>  左
.right =>  右

<script>
	var vm = new Vue({
        el:"#app",
        methods: {
              submit:function(){},
              alertMe:function(){},
        }
    })

</script>
```

# 自定义按键修饰符别名

```html
<div id="app">
    <!-- 只有点击对应65数值的键盘才会触发 -->
    <input type="text" v-on:keyup.a="halou" v-model="ha" />
</div>

<script>
    // 设置自定义按键修饰符
    Vue.config.keyCodes.a = 65;

    let app = new Vue({
        el: '#app',
        data: {
            ha: '',
        },
        methods: {
            halou: function() {
                //   获取点击的对应数值
                console.log(event.keyCode);
            }
        }
    });
</script>
```

# v-model的本质数据的双向绑定

```html
<!-- 这三种实现的效果是相同的 -->

<!-- 本质就是v-bind和v-on:input的结合-->
<input type="text" :value="san" v-on:input="sun" />
<!-- 直接将实现数据绑定的语句写在v-on:input中 -->
<input type="text" :value="san" v-on:input="san=$event.target.value" />
<!-- 直接使用v-model指令也可实现此操作 -->
<input type="text" v-model="san" />
<script>
    const vue = new Vue({
        el: '#gu',
        data: {
            san: 'nihao',
        },
        methods: {
            sun: function (event) {
                //  将input框内输入的值赋值给san
                this.san = event.target.value;
            },
        },
    });
</script>
```

# 样式绑定

## class样式

```html
1.对象绑定
				样式名:是否显示(true or false)
      <div :class="{ni: isxian,yan: ischu}">你好</div>
        data: {
			//true 为显示该样式
              isxian: true,
              ischu: true,
            }
2.数组绑定
				
      <div :class="[niclass,yanclass]"></div>
 	data: {
		自己命名:样式名
          niclass: "ni",
          yanclass: "yan",
        }
3.结合绑定
      <div :class="[niclass,yanclass,{zi: ziclass}]">你好不好</div>
  	data: {
          niclass: "ni",
          yanclass: "yan",
          ziclass: true,
        },
```

## style样式

```html
 <!-- 对象形式 -->
      <div :style="{color: hong,fontSize: daxiao}">星期日</div>
  	data: {
          hong: "red",
          daxiao: "50px",
        }

<!-- 将数据以对象的形式存在data中 -->
      <div :style="duixiang">海皮</div>
  		data: {
          duixiang: {
            color: "blue",
            fontSize: "50px",
          },
        }
```

# 分支结构

```html
判断da符合哪个条件返回哪条数据
<div v-if="da>=90">你真棒</div>
<div v-else-if="da<90 && da>=80">加油</div>
<div v-else-if="da<80 && da>=60">努力</div>
<div v-else>退学吧</div>
    data: {
        da: 20,
    },

跟据fl的值true或false编译行内样式
相当于dispaly:none or black
<div v-show="fl">你好</div>
     data: {
       	fl: true,
     },

v-if与v-show的区别
v-if控制元素是否渲染到页面
v-show控制元素是否显示(已经渲染到页面了)
```

# 循环结构

```html
	v-for =	值  in 数组
<li v-for="key in yin">{{key}}</li>
	key:值  i:索引
<li v-for="(key,i) in yin">{{key + ' ---- ' + i}}</li>

    data: {
        yin: ["陈志朋", "苏有朋", "吴奇隆", "许嵩", "汪苏泷"],
    }
```

# 遍历对象

```html
		(值，键，id) in 对象
<div v-for="(v,k,i) in hu">{{v+'--'+k+'--'+i}}</div>
 data: {
        hu: {
        name: "吴奇隆",
        uname: "霹雳虎",
        ge: "《祝你一路顺风》",
        },
	},
```

# vue常用特性

1. ## 表单操作

   v-model.number   转为数值

   v-model.trim         去除两端空格

   v-model.lazy          失去焦点触发

   ```html
   <input v-model.number='num' type='text'>
   <input v-model.trim='num' type='text'>
   <input v-model.lazy='num' type='text'>
   ```

2. ## 自定义指令

   ```html
   //使用的时候是 自定义指令名前边加 v-
    <input type="text" v-自定义指令名 />
   Vue.directive('自定义指令名',{
   	inserted:function(el){
   		//el表示指令所绑定的元素
   		//获取元素的焦点
   		el.focus()
   	}
   })
   
   
   //带参数的自定义指令
    <input type="text" v-自定义指令名='参数'/>
   Vue.directive('自定义指令名',{
   	inserted:function(el,自定义形参名){
   //设置背景颜色为红色
   		el.style.backgroundColor = 自定义形参.参数值
   	}
   })
   	data: {
            参数: "red",
           },
   
   
   //自定义局部指令
   const vue = new Vue({
           el: "#zhi",
           data: {
             color: "red",
           },
   		//局部指令
   		directives:{
   			自定义指令名:{
   				inserted:function(el,自定义形参名){
   					//设置背景颜色为红色
   					el.style.backgroundColor = 自定义形参.参数值
   				}
   			}
   		}
         });
   ```

3. ## 计算属性

   ```html
   <div id="ji">
       <div>{{shan}}</div>
       <!-- 直接输出方法名 -->
       <div>{{fanzhuan}}</div>
   </div>
   const vue = new Vue({
       el: "#ji",
       data: {
       	shan: "nongfu",
       },
   	//计算属性必须写在computed属性中，具有缓存作用下次调用直接从缓存中取不会再次运行，节省运行效率
       computed: {
           // 自定义方法名
           fanzhuan: function () {
               // 将shan转换为数组以空格隔开，进行反转数组，将反转后的数组组成字符串
               return this.shan.split("").reverse().join("");
           },
       },
   });
   ```

4. ## 过滤器

   ```html
   <input type="text" v-model="msg">
   //使用过滤器
   <div>{{msg | shou}}</div>
   //过滤器可以多个一起使用
   <div>{{msg | shou | xiao}}</div>
   自定义过滤器
   Vue.filter('过滤器名称',function(val){
   	teturn val.chartAt(0).toUpperCase+val.slice(1)
   //获取首字母，将首字母改为大写 + 从第二个字符开始获取数据 组成一个新的字符返回出去
   })
   
   //局部过滤器
   const vue = new Vue({
       el: '#guo',
       data: {
       	msg: ''
       },
   //定义局部过滤器
       filters: {
           xiao: function (val) {
               // 获取首字母并转换为小写  拼接上 从第二个字符往后截取的值
               return val.charAt(0).toLowerCase() + val.slice(1)
           }
       }
   })
   ```

5. ## 侦听器

   应用场景：数据变化是执行异步或开销较大的操作

   ```html
   用户名：<input type="text" v-model="uname" /><br />
   年龄：<input type="text" v-model="age" />
   <div>{{pin}}</div>
   const vue = new Vue({
       el: "#zhen",
       data: {
           uname: "zhang",
           age: 18,
           pin: "zhang 18",
       },
       // 侦听器写在watch属性中
       watch: {
           // 方法名一般都是需要监听的属性
           uname: function (val) {
           	this.pin = val + "" + this.age;
           },
           age: function (val) {
           	this.pin = this.uname + "" + val;
           },
       },
   });
   ```

6. ## 生命周期

   八个生命周期

   生命周期函数无需调用，无需定义，它会按照生命周期的进程自动依次进行

   ![image-20210602101010557](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210602101010557.png)

   这些函数与data平级，前四个只要运行界面就会输出

   数据发生变化会运行5,6

   数据销毁会运行7,8      销毁是失去响应，不是删除数据

​				![image-20210602101653453](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210602101653453.png)

# 数组变异方法

```js
add: function () {
    // 在数组的最后添加元素
    this.msg.push('hi')
},
del: function () {
    // 删除数组最后一个元素
    this.msg.pop()
},
delone: function () {
    // 删除数组第一个元素
    this.msg.shift()
},
addone: function () {
    // 往数组最前边添加元素
    this.msg.unshift('哈哈')
},
splice: function () {
    // 有三个参数
    // 第一个为删除的元素的下表
    // 第二个为删除几个
    // 第三个为删除后再原位置替换的值
    this.msg.splice(2, 1, 'word')
},
sort: function () {
    // 从小到大排序
    this.msg.sort()
},
reverse: function () {
    // 反转数组
    this.msg.reverse()
}
```

# 替换数组

不会改变原始数组，返回新数组

![image-20210602114618729](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210602114618729.png)

```js

```

# 动态响应式数据

```js
const vue = new Vue({
    el:'#ni',
    data:{
        info:{
            name:'zs',
            age:'21'
        }
    }
})
//这样修改后能直接渲染到页面
vue.info.name='ls'
//直接添加属性的话不会直接渲染到页面
vue.info.sex = '男'
//也可以这样写
vue.$set(vue.info,'sex','男')
```

# 全局组件

组件特点：独立性，

```js
将组件名称当做html标签使用
组件的命名可以是驼峰式命名法  也可以使用-的形式
驼峰式命名只能在字符串模板中使用，在html中要改为-的形式，且组件名必须为小写
<组件名称></组件名称>
Vue.component('组件名称',{
    //组件中的data节点必须是函数，且返回对象
    data:function(){
        return {}
    },
    //组件模板内容必须是单个根元素，且可以是模板字符串
    template:''
})
```

# 局部组件

```js
局部组件只能在注册它的父组件中使用
```

# 父组件向子组件传值

![image-20210603092912356](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210603092912356.png)

```html
```

![image-20210603103857190](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210603103857190.png)

# 插槽

```js
<div id="cha">
    //将你好传到slot标签中  会自动覆盖里边的默认内容
    <cha-chao>你好</cha-chao>
	//不写的话显示默认内容
	<cha-chao></cha-chao>
</div>
Vue.component("cha-chao", {
        template: `
            <div>
                <strong>error!</strong>
                <slot>默认内容</slot>
            </div>
    	`,
});
const vue = new Vue({
    el: "#cha",
    data: {},
});
```

# Promise的用法

实例化Promise对象，构造函数中传递函数，该函数用来处理异步任务

resolve和reject两个参数用于处理成功和失败两种情况，通过p.then 获取处理结果

```js
//   他的形参是两个方法
var p = new Promise(function (resolve, reject) {
    if (false) {
        // 成功调用resolve
        resolve("我是正常的");
    } else {
        // 不成功调用reject
        reject("我是不正常的");
    }
});
//   then方法里有两个函数,第一个为成功的函数,第二个为失败的函数
//   里边的参数是上边resolve或reject携带的实参
p.then(
    function (data) {
        console.log(data);
    },
    function (error) {
        console.log(error);
    }
);
```

# Promise处理ajax

```js
function query(url) {
    var p = new Promise(function (resolve, reject) {
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function () {
            if (xhr.readyState != 4) return;
            if (xhr.readyState == 4 && xhr.status == 200) {
                //成功
                resolve(xhr.responseText);
            } else {
                //失败
                reject("服务器错误");
            }
        };
        xhr.open("get", url);
        xhr.send(null);
    });
    return p;
}
query("http://localhost:3000/data").then(
    function (data) {
        console.log(data);
    },
    function (info) {
        console.log(info);
    }
);
// promise不是用来发起请求的，是用来优化异步任务
// 将之前为了保证一步顺序写成的回调地狱优化成链式调用
query("http://localhost:3000/data")
    .then(function (data) {
    console.log(data);
    return query("http://localhost:3000/data1");
})
    .then(function (data) {
    console.log(data);
    return query("http://localhost:3000/data2");
})
    .then(function (data) {
    console.log(data);
});
```

# Promise常用API

```js
.then()//成功
.catch()//失败
.finally()//成功或失败都会执行
```



# axios用法

是基于Promise用于浏览器和node.js的HTTP客户端

特征：

1. 支持node.js和浏览器
2. 支持promise
3. 能拦截请求和相应
4. 自动切换json数据

```js
//
axios.get('服务器地址').then(function(ret){
    //ret为形参
    //data属性名是固定的用于获取后台响应数据
    console.log(ret.data)
})
```

# axios常用API

```js
// get请求
axios.get("http://localhost:3000/axios?id=123").then(function (ret) {
    console.log(ret);
});
//   第二种get
axios.get("http://localhost:3000/axios/id=123").then(function (ret) {
    console.log(ret);
});
//   第三种get之params传递
axios
    .get("http://localhost:3000/axios", {
    params: {
        id: 789,
    },
})
    .then(function (ret) {
    console.log(ret);
});
// delete传递参数 和get请求的三种类型相同只要将.get改为.delete就行了
axios
    .delete("http://localhost:3000/axios", {
    params: { id: 147 },
})
    .then(function (ret) {
    console.log(ret);
});
// post传递参数
axios
    .post("http://localhost:3000/axios", {
    uname: "ls",
    pwd: 147258,
})
    .then(function (ret) {
    console.log(ret);
});
// 第二种post传递方式
var params = new URLSearchParams();
params.append("uname", "zs");
params.append("pwd", "000000");
axios.post("http://localhost:3000/axios", params).then(function (ret) {
    console.log(ret);
});
//   put传递参数
axios
    .put("http://localhost:3000/axios/889", {
    uname: "ls",
    pwd: 147258,
})
    .then(function (ret) {
    console.log(ret);
});
```

# axios的全局配置

```js
// 超时时间
axios.defaults.timeout = 3000;
//   设置请求头
axios.defaults.headers["mytoken"] = "nihao";
//   配置请求基准url
axios.defaults.baseURL = "http://localhost:3000/";

axios.get("axios-json").then(function (ret) {
    console.log(ret.data);
});
```

# axios拦截器

```js
//请求拦截器
在请求发出之前设置信息
axios.interceptors.request.use(
    function (config) {
        console.log(config.url);
        config.headers.mytoken = "halou";
        return config;
    },
    function (err) {
        console.log(err);
    }
);
axios.get("http://localhost:3000/adata").then(function (data) {
    console.log(data);
});
//响应拦截器
在获取数据之前对数据做一些加工处理
axios.interceptors.response.use(
    function (res) {
        return res.data;
    },
    function (err) {
        console.log(err);
    }
);
axios.get("http://localhost:3000/adata").then(function (data) {
    console.log(data);
});
```

# async/await的基本用法

![image-20210605192000961](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210605192000961.png)

```js
//处理单个异步请求
axios.defaults.baseURL = "http://localhost:3000";
// axios.get("adata").then(function (ret) {
//   console.log(ret.data);
// });

// async function query() {
//   var ret = await axios.get("adata");
//   console.log(ret);
// }
async function query() {
    var ret = await new Promise(function (resolve, reject) {
        setTimeout(function () {
            resolve("nihao");
        }, 1000);
    });
    console.log(ret);
}
query();
```















