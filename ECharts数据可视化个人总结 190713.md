# ECharts的基本使用

### ECharts使用五部曲

1. 下载并引入echarts.js文件

2. 准备一个具备大小的盒子         放生成的图表

3. 初始化echarts的实例对象         实例化echarts对象

4. 指定配置项和数据                     根据需求修改配置选项

5. 将配置项设置给echarts的实例对象     让echarts对象根据修改好的配置生效

   ```css
   <!DOCTYPE html>
   <html>
   <head>
       <meta charset="utf-8">
       <title>ECharts</title>
       <!-- 引入 echarts.js -->
       <script src="echarts.min.js"></script>
   </head>
   <body>
       <!-- 为ECharts准备一个具备大小（宽高）的Dom -->
       <div id="main" style="width: 600px;height:400px;"></div>
       <script type="text/javascript">
           // 基于准备好的dom，初始化echarts实例
           var myChart = echarts.init(document.getElementById('main'));
   
           // 指定图表的配置项和数据
           var option = {
               title: {
                   text: 'ECharts 入门示例'
               },
               tooltip: {},
               legend: {
                   data:['销量']
               },
               xAxis: {
                   data: ["衬衫","羊毛衫","雪纺衫","裤子","高跟鞋","袜子"]
               },
               yAxis: {},
               series: [{
                   name: '销量',
                   type: 'bar',
                   data: [5, 20, 36, 10, 10, 20]
               }]
           };
   
           // 使用刚指定的配置项和数据显示图表。
           myChart.setOption(option);
       </script>
   </body>
   </html>
   ```

   ### 相关配置属性

   ![image-20210422144259360](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210422144259360.png)

   

### 边框图片

![image-20210422190130377](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210422190130377.png)

![image-20210422190421611](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210422190421611.png)