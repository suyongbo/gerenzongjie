#  Git 笔记

Git是一个开源的分布式版本控制系统是目前世界上最先进最流行的版本控制系统可以快速的处理从很小到很大的项目版本管理

特点：项目越大越复杂，协同开发者越多，越能体现出Git的高性能和高可用性

## Git的三个区域

1.  工作区域     处理工作的区域
2.  暂存区域       已完成的工作的临时存放区域等待被提交
3.  Git仓库          最终的存放区域

## Git的三种状态

![image-20210509190603541](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210509190603541.png)

Git操作的结果是让工作区的文件都处于未修改状态

# 1.仓库初始化

```shell
git init
```

# 2.查看状态

```shell
git status 
git status -s #精简展示
```

# 3.添加到暂存区

```shell
git add 文件名字
git add .  # 添加多个到暂存区
```

# 4.提交到仓库

```shell
git commit -m '文件描述'
git commit -a -m '文件描述' #跳过暂存区,必须是已经追踪过的文件
# 修改上次提交的描述信息 重新提交  使用 :wq 退出vim编译器
git commit --amend 
 
如果提交报Please tell me who you are.错  
解决方法：在命令行中执行
git config --global user.email "你的邮箱" 
git config --global user.name "你的名字"
```

# 4.1撤销对文件的修改   很少用

```shell
git checkout -- 文件名
```

# 5.取消暂存的文件 

```shell
git reset HEAD 要移出的文件名称
```

#  6.移除文件

```shell
# 从 Git仓库和工作区中同时移除 index.js 文件
git rm -f index.js
# 只从 Git 仓库中移除 index.css，但保留工作区中的 index.css 文件
git rm --cached index.css
```

# 7.查看提交历史

```shell
# 按时间先后顺序列出所有的提交历史，最近的提交在最上面
git log

# 只展示最新的两条提交历史，数字可以按需进行填写
git log -2

# 在一行上展示最近两条提交历史的信息
git log -2 --pretty=oneline

# 在一行上展示最近两条提交历史信息，并自定义输出的格式
# &h 提交的简写哈希值  %an 作者名字  %ar 作者修订日志  %s 提交说明
git log -2 --pretty=format:"%h | %an | %ar | %s"
```

# 8.回退到指定的版本

```shell
# 在一行上展示所有的提交历史
git log --pretty=oneline

# 使用 git reset --hard 命令，根据指定的提交 ID 回退到指定版本
git reset --hard <CommitID> 

# 在旧版本中使用 git reflog --pretty=oneline 命令，查看命令操作的历史
git reflog --pretty=onelone

# 再次根据最新的提交 ID，跳转到最新的版本
git reset --hard <CommitID>
```

# 9.设置远程仓库

```shell
首次建立与远程仓库的链接
git remote add origin 仓库url链接
git push -u origin master
再次提交就直接使用下边这句就行
git push
```

# 10.从git网页下载文档

```shell
点击网址上的下载  选择ssh 复制链接
git clone 复制的链接 
指定本地仓库的目录
git clone 复制的链接 本地目录
-b 指定克隆的分支 不指定默认为master
git clone 复制的链接 -b 分支名称 本地目录
```

# 11.查看分支列表

```shell
git branch
```

# 12.创建分支

```shell
创建分支的时候必须先切换到master主分支上
git branch 新建分支名称

# 原分支名称可不写 不写时为当前分支 更改名称
git branch -m 原分支名称 新分支名称

# 原分支名称可不写 不写时为当前分支 强制 更改名称
git branch -M 原分支名称 新分支名称

# 删除本地分支 （自身所在的分支不能进行删除） 
git branch -d 分支名称

# 强制删除本地分支 （自身所在的分支不能进行删除） 
git branch -D 分支名称
```

# 13.切换分支

```shell
必须把所有文件提交到仓库后才能切换分支
git checkout 分支名称
```

# 14.快速创建分支并直接切换分支

```shell
git checkout -b 分支名称
```

# 15.合并分支

```shell
合并分支时     需要先切换到master分支再进行合并
git merge 需要合并分支的名称
```

# 16.删除分支

```shell
要删除的分支必须是合并后的分支，要不然删除不了
git branch -d 需要删除的分支名称 未合并的分支不能删除
git branch -D 需要删除的分支名称 未合并的分支也可以强制删除
```

# 17.遇到冲突时的分支冲突

需要手动打开冲突的文件进行修改

```shell
手动解决冲突后
git add .
git commit -m "**"
```

# 18.将本地分支推送到远程仓库分支

```shell
第一次                       远程分支名称可写可不写
git push -u 远程仓库别名 本地分支名称:远程分支名称
例如：
git push -u origin login:pay
```

# 19.查看远程仓库所有分支列表

```shell
git remote show 远程仓库分支
//列出远程仓库的详细信息 （仓库地址）
git remote -v
//添加远程仓库
git remote add 远程仓库别名 远程仓库的url地址
//修改远程仓库的别名
git remote rename 需要更改的仓库别名 新的仓库别名
# 删除指定的远程仓库  (慎用)
git remote remove 仓库别名 
# 修改远程仓库的url地址 （多在仓库迁移后，地址发生变化后使用)
git remote set-url 远程仓库别名 新的仓库地址
```

# 20.远程仓库的分支下到本地

![image-20210511141858253](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210511141858253.png)

# 21.更新代码

``` shell
从远程仓库获取最新版本并合并到本地。
git pull   
然后执行 git merge，把获取的分支的 HEAD 合并到当前分支。
git merge 
```

# 22.删除远程仓库的分支

```shell
git push 远程仓库名字 --delete 远程仓库分支名称
``` 

# 23.以高亮或彩色方式显示改动状态

```shell
git config --global color.ui true
git diff  对比修改的内容
# 比较暂存区中的文件和上次提交时的差异
git diff --cached  或  git diff --staged
# 比较指定版本之后改动的内容  commit ID 是 git log 获取到的 id 
git diff (commit ID)
# 比较两个分支之间的差异
git diff 分支名称 分支名称
# 比较两个分支分开后各自的改动内容
git diff 分支名称...分支名称
```
# 24.把已经提交的记录合并到当前分支
``` shell
git cherry-pick <commit ID>
# 常用于解决冲突
git add .
# 使用 --continue 让 cherry-pick继续执行
git cherry-pick --continue
# 放弃合并 回到操作前的样子
git cherry-pick --abort
# 退出cherry-pick 不回到操作前的样子 
git cherry-pick --quit
```

# 25.撤销某次提交 此次提交前的所有提交都会被保留
```shell
# commit ID 是 git log 获取到的 id 
git revert <commit ID> 
```
# 26.已经上传了某个文件到git仓库中后，添加 .gitignore 
```shell
# 清除远程分支中的所有文件  或  清除单个文件
git rm -r --cached .  或  git rm -r --cached 文件名

git add .
git commit -m "update .gitignore"
git push origin master
```