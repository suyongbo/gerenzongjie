#  jQuery中的Ajax

Ajax主要用于发送请求接收数据

### $.get()函数语法

![image-20210503111425912](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210503111425912.png)

### $.get()发起不带参数的请求

```js
<button id="yescan">带参数的</button>
<button id="nocan">不带参数的</button>
$('#nocan').on('click', function () {
    //   不带参数的话返回所有的值
    // 请求地址                                         请求成功后的回调函数
    $.get('http://www.liulongbin.top:3006/api/getbooks', function (res) {
        console.log(res);
    });
});
```

### $.get()发起带参数的请求

```js
$('#yescan').on('click', function () {
    $.get(
        // url请求地址
        'http://www.liulongbin.top:3006/api/getbooks',
        //   对象{id: }  根据id进行查询并返回当前id的值
        { id: 3 },
        //   res就是服务器返回的值
        function (res) {
            //   输出返回的值
            console.log(res);
        }
    );
});
```

### $.ajax()可以发送get()和post()的请求方式

```js
发送post语法：  
$.ajax({
    type:'post',//请求方式  post
    url:'http://www.liulongbin.top:3006/api/getbooks',//请求的url地址
    data:{},//请求要携带的数据   如果没有数据可以不写或为空
    success:function(res){
        console.log(res);
    }//请求成功后的回调函数
})
```

### 监听表单提交事件

```js
submit给form表单创建
1.通过  form表单.submit(function(e){
    e.perventDefault()//阻止默认提交行为
})     进行监听
2.通过 form表单.on('submit',function(e){
    $(this).serialize()获取表单中设置name属性的字段
    form表单.reset()//重置form内所有表单数据
        e.perventDefault()
}) 
```

### 模板引擎

根据程序员指定的模板结构和数据自动生成一个HTML页面

好处：

1. 减少字符串的拼接操作
2. 使代码结构更清晰
3. 使代码易于阅读与维护

![image-20210505160005053](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210505160005053.png)

### 输出带标签的语句

```js
{{@ 对象}}
```

### if判断

```js
{{if 判断条件}}
    alert(符合if条件输出)
{{else if 判断条件}}
    alert(符合else if 条件输出)
{{/if}}
```

### 循环

```js
使用each进行循环  属性
语法：
{{each 循环对象的属性}} 
使用{{$value}}获取循环的值
使用{{$index}}获取循环的索引
{{/each}}
<ul>
    {{each xun}}
        <li>循环的值{{$value.name}}</li>
		<li>索引 {{$index}}</li>
	{{/each}}
 </ul>
```

### 过滤器

```js
调用过滤器语法：   {{需要处理的值 传递参数 | 函数名}}
{{reg | fn}}
定义过滤器语法:
template.defaults.inports.函数名 = function(接收参数){
    //需要 return  处理的结果·
}
template.defaults.inports.fn = function(reg){
    return alert(reg)
}
```

### 正则与字符串操作

1. 基本语法

   exec()函数用于检索字符串中的正则表达式的匹配

   如果有匹配的值返回匹配的值，否则返回null

   ```js
   语法：正则表达式.exec(字符串)
   var str = 'hello';
   var pat = /o/
   //              索引      匹配的字符串
   //输出的结果['o',index:4 input:hello griups:undefined]
   console.log(pat.exec(str))
   
   ```

2. 分组

   正则表达式中()包括起来的内容表示一个分组，可以通过分组来提取自己想要的内容

   ```js
   var str = `<div>我是{{name}}</div>`;
   var zhengze = /{{([a-zA-Z]+)}}/;
   console.log(zhengze.exec(str));
   //  匹配的值   提取到的值  索引
   // ["{{name}}", "name", index: 7, input: "<div>我是{{name}}</div>", groups: undefined]
   ```

3. 字符串的replace函数

   replace()函数用于在字符串中用一些字符替换一些字符

   ```js
   语法： 字符串.replace('被替换的值','替换的值')  返回一个替换后的字符串
   var str = `<div>我是{{name}}</div>`;
   var zhengze = /{{([a-zA-Z]+)}}/;
   var huoqu = zhengze.exec(str);
   //  匹配的值 0  提取到的值1  索引
   // ["{{name}}", "name", index: 7, input: "<div>我是{{name}}</div>", groups: undefined]
   console.log(str.replace(huoqu[0], huoqu[1]));
   //<div>我是name</div>
   ```

4. 使用while循环多次的replace()

   ```js
   var data = {
       name: '张小军',
       age: 18,
   };
   var str = `<div>你好我是{{name}}，今年{{age}}岁了</div>`;
   var zhengze = /{{\s*([a-zA-Z]+)\s*}}/;
   var tem = null;
   //因为zhengze.exec(str)不能找到返回null 并且while循环的时候null为false
   while ((tem = zhengze.exec(str))) {
       str = str.replace(tem[0], data[tem[1]]);
   }
   console.log(str);
   //<div>你好我是张小军，今年18岁了</div>
   ```

### XMLHttpRequset简称xhr

1. 使用xhr发起get请求

   ```js
   // 创建xhr对象
   var xhr = new XMLHttpRequest();
   console.log(xhr.readyState);  0
   //   调用open函数
   xhr.open('get', 'http://www.liulongbin.top:3006/api/getbooks');
   console.log(xhr.readyState);  1
   //   调用send函数  发送请求
   xhr.send();
   //   4.监听 onreadystatechange 事件
   xhr.onreadystatechange = function () {
       console.log(xhr.readyState);  2,3,4
       //readyState === 4  4代表数据解析完成可以使用
       //status ===200  	200代表联网成功
       if (xhr.readyState == 4 && xhr.status == 200) {
           // 返回的是一个字符串
           console.log(xhr.responseText);
       }
   };
   ```

2. xhr对象的readyState属性  易考点

   ![image-20210506114305955](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210506114305955.png)

3. 使用xhr发起带参数的get请求  也叫查询字符串

   ```js
   xhr.open('get', 'http://www.liulongbin.top:3006/api/getbooks?id=1&name=张三');
   //?id=1就是携带的参数，多个参数用&符号进行拼接
   ```

4. url中的编码与解码

   ```js
   编码使用 encodeURI(字符串)//返回编译后的字符串
   解码使用 decodeURI(编码)//返回解码后的字符串
   ```

5. 使用xhr发送post请求

   ```js
   // 1.创建xhr对象
   var xhr = new XMLHttpRequest();
   // 2.调用open函数
   xhr.open('post', 'http://www.liulongbin.top:3006/api/addbook');
   //3.设置Content-Type属性固定写法  设置请求头
   xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
   //4.调用send()同时传递数据
   xhr.send('bookname=世界&author=施耐庵&publisher=我出版');
   //5.监听 onreadystatechange 事件
   xhr.onreadystatechange = function () {
       if (xhr.readyState == 4 && xhr.status == 200) {
           console.log(xhr.responseText);
       }
   };
   ```

### JSON

1. json是javascript对象和数组的字符串表示法，它使用文本表示一个js对象或数组的信息。
2. json的本质是字符串
3. json是一种轻量级的文本数据交换格式
4. json比xml更快、更小、更容易解析

### json的两种结构

1. 对象结构

   ​	对象结构在json中表示为{}括起来的内容

   ​	数据结构为{key:value,key:value,…}的键值对结构

   ​	key必须使用英文的双引号包括

   ​	value的数据类型可以是数字、字符串、布尔值、null、数组、对象六种

2. 数组结构

   ​	数组结构在json中表示[]括起来的内容

   ​	数据结构为["java",20,true…]

   ​	数组中数据的类型可以是数字、字符串、布尔值、null、数组、对象六种

### json语法注意事项

![image-20210506184051222](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210506184051222.png)



### 创建FormData

![image-20210506163904000](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210506163904000.png)

### 同源反之则是跨域

![image-20210508140702432](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210508140702432.png)

### jsonp的原理  面试会问

jsonp是json的一种"使用模式" 可以用于解决主流浏览器的跨域数据访问

![image-20210508142214993](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210508142214993.png)

### 使用ajax发起jsonp请求

```js
$.ajax({
    //   路径
    url: 'http://liulongbin.top:3006/api/jsonp',
    // 发起的jsonp请求
    dataType: 'jsonp',
    //
    jsonp: 'callback',
    //
    jsonpCallback: 'abc',
    // 回调函数
    success: function (res) {
        console.log(res);
    },
});
```

### 每次调用ajax时 默认调用$.ajaxPrefilter函数

```js
// 每次调用$.ajax()时 会先调用这个函数
// 在这里可以拿到我们给Ajax提供的配置对象
$.ajaxPrefilter(function (opt) {
    opt.url = http://api-breakingnews-web.itheima.net${*opt*.url};
});
```

### 模板引擎

```js
// 将数据和模板进行关联    template('模板的id  不用带#', 对象);
// str为生成的html结构
let str = template('contain', data);
// 将生成好的html结构展示到界面上
$('#container').html(str);
```

### 防抖 

当事件被触发后延迟n秒后再执行调用，如果在这n秒内事件又被触发则重新计时

### 节流

可以减少一段时间内的事件的触发频率

