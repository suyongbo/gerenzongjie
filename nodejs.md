# node

```html
    https://nodejs.org/zh-cn/  下载node

    node -v 查看node版本

    node js文件   在node中运行js文件

```

# fs文件系统模块

```js
// 引入fs文件
const fs = require('fs');

fs.readFile('路径','编码格式','回调函数')用来读取指定文件中的内容
例如:
fs.readFile('./112.txt', 'utf8', function (err, data) {
    // 读取失败进入
    if (err) {
        console.log(err.message);
    }
    //输出失败结果   
    // 读取成功 err为null则为成功
    // 读取失败 err为错误对象
    console.log(err); 
    //输出成功
    console.log(data);
});

// 📢：写入的数据会覆盖文件中原来的数据
// 📢：如果指定的文件不存在会自动创建改文件并将内容写入
fs.writeFile('路径','写入的内容','字符编码(选填)','回调函数')用来向指定的文件中写入内容
例如:
fs.writeFile('./112.txt', '2020-5-18', function (err) {
  console.log(err);
  // 写入失败
  if (err) {
    return console.log(err.message);
  }
  // 写入成功
  console.log('写入成功');
});
```

出现路径拼接错误：

1. 因为提供的是相对路径

解：

1. 提供完整的文件存放路径     										移植性差，不利于维护

2. 使用'__dirname'  它代表当前文件所处到的目录      不够严谨

   ![image-20210518102800924](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210518102800924.png)

   ```js
    fs.readFile(__dirname + '/112.txt', 'utf8', function (err, data) {
        // 读取失败进入
        if (err) {
            console.log(err.message);
        }
        //输出失败结果   
        // 读取成功 err为null则为成功
        // 读取失败 err为错误对象
        console.log(err); 
        //输出成功
        console.log(data);
    });
   ```

3. 使用path路径模块

   1. path.join()   用来将多个路径片段拼接成一个完整的路径字符串

      ```js
      需要先引入path    更多用它
      const path = require('path');
      ```

      ![image-20210518103604853](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210518103604853.png)
        
        ```js
           // path.join(__dirname,'/112.txt') 用来拼接路径  之后尽量都是要这种ch
            fs.readFile(path.join(__dirname,'/112.txt'),'utf8',function(err,data){
                 // 读取失败进入
                if (err) {
                    console.log(err.message);
                }
                //输出失败结果   
                // 读取成功 err为null则为成功
                // 读取失败 err为错误对象
                console.log(err); 
                //输出成功
                console.log(data);
            })
        ```

   2. path.basename()  获取路径中的文件名      了解就可以

      只有一个参数返回的文件名带有后缀名  

       两个参数返回不带后缀名的文件名

      

   3. path.extname()获取路径的扩展名     

      ![image-20210518104458651](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210518104458651.png)

# http模块

```js
创建web服务器的四部曲
//第一步引入http
const http = require('http')
//第二步创建web服务器实例
const server = http.createServer()
//第三步为服务器实例绑定，request事件 监听客户端的请求
server.on('request',function(req,res){
    req请求对象  包含了与客户端相关数据和属性
    res响应对象	 
    //客户端请求的地址
    const url = req.url
    //客户端请求的类型
    const method = req.method
    // 解决中文乱码问题
    res.setHeader('Content-Type', 'text/html; charset=utf8');
    //向客户端响应内容
    res.end('hello 永波');
})
//第四步启动服务器
server.listen(80,function(){
    console.log('running at http://localhost:80')
})
```

# node.js中的模块化

### 向外共享模块作用域中的成员

1. module对象

   - 通过module.exports对象将成员共享出去，默认为空

   ```js
   例如：
   给该对象赋值 可以赋值方法，变量，直接赋值都是可以的
   module.exports.age = 12
   ```

   - 另一个界面使用require()进行接收，接收的就是module.exports对象

   ```js
   接收界面
   const m = require('接收的页面')
   console.log(m)
   输出结果就是：
   {age:12}
   ```

2. exports默认也是空对象

   - module.exports和exports指向同一个对象时，两个可以混用
   - module.exports和exports不指向同一个对象时，以module.exports为主
   ```js
        //不写  module  也是可以的向外共享数据的  输出的是name和 age
        exports.name = 'ls'
        module.exports.age = 12
        --------
        // 但是自定义模板有  module.exports  的时候 还是会以此为主 输出的只有 aa 没有  name 
        exports.name = 'ls'
        module.exports = {
            aa:'zs'
        }

        --------
        //这种情况下得到的是  name 
        //exports的权限永远没有module.exports的大  
        // 📢：这里的exports是  直接等于一个对象  和第一种是不一样的
        module.exports.name = 'ls'
        exports = {
            aa:'zs'
        }

   ```

# 使用express创建服务器

```js
//第一步需要先下载express包
npm i express@4.17.1
// 1.导入模板
const express = require('express');
// 2..创建服务器
const app = express();

//托管静态资源  文件名下的所有文件都可以通过服务器进行访问
//可以托管多个静态资源  但是它的运行顺序是自上而下的
//如果两个文件夹里都有同一个名字的文件那么会显示第一个里边的文件
app.use(express.static('文件名'))

// 加载json解析  只能解析json类型的参数
app.use(express.json())

// 使用urlencoded 解析form-urlencoded类型的参数  不解析同样为空
app.use(express.urlencoded({extended:false}))


// 3.设置监听
// 第一个参数是客户端请求的路径 如果文件名是index.js则可以不写
app.get('/', (req, res) => {
    //向客户端响应内容
    res.send('你好 ！！！');
    //req.query获取客户端发送的查询参数，默认为空
    //后期根据它对象里的值进行判断数据是否符合规范s
    res.send(req.query)
   
});
//:后边的为需要动态匹配的参数，可以跟多个
//id和name都属于动态参数  并且，动态参数的值不能为空
app.get('/api/:id/:name', (req, res) => {
    //req.params动态匹配到的url参数值，默认为空
    res.send(req.params)
});


// body是用户传递的参数  如果不加载内置中间件进行解析 算携带了参数  返回的也是 undefined
app.post('/lipk',(req,res)=>{
    console.log(req.body)
    res.send(req.body)
})

// 注意错误中间件必须写到所有路由之后  参数有四个  第一个永远是err  
// 错误中间件作用是防止出现错误后服务器崩溃
app.use((err,req,res,next)=>{
    console.log(err.message)
    res.send(err.message)
    next()
})

// 4.启动服务器
app.listen(88, () => {
    console.log('http://localhost:88');
});
```

# express路由

路由指客户端的请求域服务器处理函数之间的映射关系

它由三部分组成：请求类型、请求的url地址、处理函数

### 创建路由模块文件

```js
// 1.导入模板
const express = require('express');
// 2..创建路由对象
const router = express.Router();
// 挂载具体的路由
router.get('/list',(req,res)=>{
    res.send('get')
})
router.post('/add',(req,res)=>{
    res.send('post')
})
// 4.导出路由对象
module.exports = router

```

### 导入路由模块

```js
// 1.导入模板
const express = require('express');
// 2..创建服务器
const app = express();

// 1.1导入路由
const router = requice('路由模块的路径')
//1.2注册路由模块   app.use(路由前缀(前缀可写可不写)，文件)用来注册全局中间件
app.use(router)

// 4.启动服务器
app.listen(88, () => {
  console.log('http://localhost:88');
});
```


# 使用  nodemon
+ npm install -g nodemon 将nodemon第三方包安装到全局中
+ 解决运行node服务器时稍有修改就要重新启动项目问题  
+ 安装后只需运行一次 修改项目后不用重新启动项目，会自动更新
+ 运行命令由 -- node 文件 -- 改为 -- nodemon 文件 --


# 解决cors跨域问题

```js
//1.下载cors
npm i cors
//2.配置跨域中间件  配置它必须要在路由之前
const cors = require('cors')
//3.注册跨域
app.use(cors())
```

# sql的配置

```js
// 导入mysql模板
const mysql = require('mysql');
//连接数据库
const db = mysql.createPool({
    host: 'localhost', 		//数据库的ip
    user: 'root', 			//数据库的账户
    password: '123456',	 	//数据库的密码
    databases: 'my_db_01', 	//要操作的数据库
});
// 测试链接
	//	sql语句
db.query('select 1', (err, result) => {
    // 错误返回数据
    if (err) return console.log(err.message);
    //正确输出数据
    console.log(result);
});
```











