### JQ的入口函数

```css
1.等DOM加载完毕再执行js代码  两种方式
$(document).ready(function(){
    
})
2.
$(function(){
    
})

$相当于jQuery
这样写也行：
jQuery(function(){
    
})
```

### DOM和JQurey的相互转换

```css

DOM获取
var divs = document.querySelector('div')

JQurey获取  返回的是一个伪数组
$('div')

DOM转换JQurey
$(divs)

JQurey转换DOM
$('div')[0]

```

### JQuery选择器

```css

$('选择器')   样式和css中的写法相同  

例：
获取id：$('#选择器')   获取class：$('.选择器')

获取后代：$('父元素  子元素')  获取子代：$('父元素>子元素')

:checked  选择器    
返回的是复选框选中的元素   返回的结果是数组
```

### JQuery筛选选择器

```css

$('li:first')获取第一个li元素

$('li:last')获取最后一个li元素

$('li:eq(2)')获取索引为二的li元素  索引从0开始

$('li:odd')获取索引为奇数的元素

$('li:even')获取索引为偶数的元素

```

### JQ各种方法

```css

$('li').parent(); 查找最近的父级(亲爸爸) 只找一个

$('li').parents(); 查找所有的父级 

$('li').parents('.yeye'); 查找到他的父级中类名为yeye的 

$('ul').children(); 查找他所有的子级元素

$('ul').children('li'); 查找他的子级中标签名为li的所有元素

$('ul').find('li'); 查找他的所有后代中标签名为li的所有元素

$('.first').siblings(); 查找所有兄弟节点，不包括自己

$('.first').siblings('li'); 查找兄弟为li的节点

$('.first').nextAll(); 查找当前元素之后的所有同辈元素

$('.last').prevAll(); 查找当前元素之前的所有同辈元素

$('div').hasClass('protected'); 查找当前元素是否有这个类，有的话返回true

$('li').eq(2); 相当于$('li:eq(2)'),索引从0开始

$(this).index()获取当前元素的索引

$('li').show()显示

$('li').hide()隐藏

substr()截取字符串

toFixed()可以把namber四舍五入为指定的小数

遍历元素的两种方式：

$('div').each(function(索引号,值){})

$.each($('div'),function(索引号,值){})  可以循环数组或对象



```

### 创建元素

1. ##### 内部添加

   ```css
   $('ul').append(li)添加到子元素的最后
   $('ul').prepend(li)添加到子元素的最前
   ```

2. ##### 外部添加

   ```css
   $('ul').after(li)添加到当前元素的上边
   $('ul').before(li)添加到当前元素的下边
   ```

3. ##### 删除元素

   ```css
   $('ul').remove()删除匹配的元素  自杀
   $('ul').empty()删除匹配的元素里边的子节点
   $('ul').html('')删除匹配的元素里边的子节点
   ```

JQ插件  JQuery Code Snippets

### 	JQ排他思想

```css
<button>你1</button>
    <button>你2</button>
    <span>13</span>
    <button>你3</button>
    <button>你4</button>
    <button>你5</button>

    <script>
      	$('button').click(function () {
     //   找到点击的这个button，给它设置为蓝色背景
   		$(this).css('background', 'blue');
    // 根据点击的这个button，找到它的所有兄弟，给他们的背景设置为空
     	$(this).siblings().css('background', '');

   // 链式写法
        $(this).css('background', 'red').siblings().css('background', '');
});
    </script>
```

### css操作

```css

不给值的情况下返回设置的属性值
$('li').css('width')

给值的情况下，设置宽度
$('li').css('width',300)

css也可以一次更改多个 以对象形式传递  
以对象形式传递属性名和属性值为数字的可以不加引号 
如果是复合属性要用驼峰命名法
如：background-color  要写成backgroundColor
$('li').css({
    width:400,
    hight:400,
   	backgroundColor:'red',
})

```

### 添加类

```css
添加类不覆盖原有的
$(this).addClass('类名')

删除类
$(this).removeClass('类名')

切换类 判断是否有这个类名，有的话删掉，没有的化添加
$(this).toggleClass('类名')

```

### JQ效果

```css

隐藏和显示效果一样

show(速度，切换效果，回调函数) 这三个参数可写可不写
hide(速度，切换效果，回调函数) 这三个参数可写可不写

切换，点击显示再点击隐藏
toggle(速度，切换效果，回调函数) 这三个参数可写可不写

例：
1000单位是毫秒，swing默认可以不写 
隐藏后再调用函数
show(1000,swing,function(){
    alert('11')
})

滑动

向下滑动
slideDown(速度，切换效果，回调函数) 这三个参数可写可不写
向上滑动
slideUp(速度，切换效果，回调函数) 这三个参数可写可不写
切换上下滑动
slideToggle(速度，切换效果，回调函数) 这三个参数可写可不写

例：
1000单位是毫秒，swing默认可以不写 
下拉完成后再调用函数
slideDown(1000,swing,function(){
    alert('11')
})

事件切换    
hover(经过，离开)
hover(over,out)
例：
鼠标经过和离开的混合写法    如果只写一个函数鼠标经过离开都会触发 
hover(function(){
    alert('我是经过的')
},function(){
    alert('我是离开的')
})



淡入淡出
淡入：
fadeIn(速度，切换效果，回调函数) 这三个参数可写可不写

淡出：
fadeOut(速度，切换效果，回调函数) 这三个参数可写可不写

切换淡入淡出
fadeToggle(速度，切换效果，回调函数) 这三个参数可写可不写

设置透明度
fadeTo(速度，透明度，切换效果，回调函数) 
速度和透明度必须写   透明度从0-1

例：
先让它停止动画再赋值动画效果防止动画混乱
$(this).stop().fadeTo(400,0.5)


自定义动画
animate(对象，时间，切换效果，回调函数)

里边的对象必须写
animate({}，时间，切换效果，回调函数)

例：
对象里边什么都可以写
$(this).animate({
    	距离左侧边框
    left:400,
        距离上部边框
    top:300,
        透明度
    opacity:.5
        可以去往某一个位置
    scrolltop:0
},400)
```

### 停止动画stop()

stop()停止的是上一次的动画,必须写在动画的前面

例：

```css
$(this).children('ul').stop().slideToggle()
```

### JQ属性操作

1. 获取元素的固有属性值   也可以获取自定义属性值

   ${'div'}.prop('属性')

   

2. 设置元素属性

   ${'div'}.prop('属性','属性值')

   

3. 设置自定义属性

   $('div').attr('属性','属性值')

   

4. 获取自定义属性值  也可以获取固有属性值

   $('div').attr('属性')

   

5. 数据缓存    数据存放在元素的缓存中，不在页面显示

   $('div').data('属性','属性值')

   

6. 获取数据缓存中的属性值

   $('div').data('属性')

### JQ获取元素内容

1. 获取元素内容   相当于innerHTML  返回的带标签

   $('div').html()

   

2. 设置元素内容

   $('div').html('123')

   

3. 获取元素文本内容  相当于innerText  只返回文本

   $('div').text()

   

4. 设置元素内容  

   $('div').text('123')

   ​                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        

5. 获取表单内容   相当于.value

   $('input').val()

   

6. 设置表单内容

   $('input').val('123')

### JQ尺寸

![image-20210419164326802](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210419164326802.png)

### JQ位置

1. 获取设置距离文档的位置

   ```css
   获取
   $('div').offset()返回的是是对象 返回的有两个值1.top2.left
   
   设置
   $('div').offset({
       top:200,
       left:200,
   })
   ```

2. 获取距离带有定位父级的位置   父级没有定位返回到文档的距离

   ```css
   只能获取不能设置
   $('div').position()
   ```

3. 获取被卷曲的头部

   ```css
   获取
   $(document).scrolltop()
   
   设置  页面加载后马上跳到设置的位置  不用跟单位
   $(document).scrolltop(100)
   ```

4. 获取被卷曲的左侧

   ```css
   
   $(document).scrollleft()
   ```

### JQ事件处理

使用on()可以处理一个或多个事件  以对象的形式传递

```css
$('div').on({
    click:function(){
        
    },
     mouseenter:function(){
        
    }
})

也可以两个事件处理相同就可以这样写，两个事件都会触发这个函数
$('div').on('click mouseenter',function(){
    
})

JQ事件委托

	把加给子元素的事件绑定到父元素身上


父元素.on(点击事件，子选择器，函数(){})
$('ul').on('click','li',function(){
    this指向点击的这个li
    $(this)
})

on()也可以给未来动态创建的元素绑定的事件

父元素.on(点击事件，新创建的元素，函数(){})
$('ul').on('click','li',function(){
    
})
```

### JQ解绑事件

```css
$('div').off()//解除div的所有事件

$('div').off('click')//解除div的点击事件

$('ul').off('click','li')//解除li的绑定事件

```

### JQ触发一次事件

```css
$('div').one('click') //只能点击触发一次
```

### JQ自动触发事件

```css
1. 
元素.事件()
$('div').click()

2. 
元素.trigger('事件')
$('div').trigger('click')

3.
元素.triggerHandler('事件')  不会触发元素的默认行为
$('div').triggerHandler('click')
```

