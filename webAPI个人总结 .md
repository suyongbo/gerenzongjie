# web  APIs

### DOM 是一个文档对象模型

是一个标准编程接口处理可扩展性语言；

![image-20210406100935424](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210406100935424.png)

### 获取元素

1. 根据ID获取

   使用getElementByid()方法

   例：

   ```css
     <body>
       <div id="hao">你好呀！！！</div>
       <script>
         // 文档页面从上往下加载  所以要先有标签  要把script 写在下边
         //  get获得 element元素 by通过 
         //  参数id是大小写敏感的字符串
         var h = document.getElementById('hao');
         console.log(h);
         //  返回的是一个元素对象
         console.log(typeof h);
         // 使用dir打印返回的元素对象 更好的查看里面的属性和方法
         console.dir(h);
       </script>
     </body>
   ```

2. 根据标签名获取

   使用getElementByTagName()方法返回带有标签名的集合

   例：

   ```css
       <div>
         <span>你是不是傻1！！！</span>
         <span>你是不是傻2！！！</span>
         <span>你是不是傻3！！！</span>
         <span>你是不是傻4！！！</span>
         <span>你是不是傻5！！！</span>
       </div>
       <span id="s">
         <span>哈哈1！！！</span>
         <span>哈哈2！！！</span>
         <span>哈哈3！！！</span>
         <span>哈哈4！！！</span>
         <span>哈哈5！！！</span>
       </span>
       <script>
         // 返回的是所有标签名为span的对象集合  以为数组的形式存储
         //   var sp = document.getElementsByTagName('span');
         //   console.log(sp);
         //   console.log(sp[0]);
         //   for (var i = 0; i < sp.length; i++) {
         //     console.log(sp[i]);
         //   }
         // 只有一个span 还是以伪数组的形式返回，一个都没有返回空的伪数组
   
   
   
         //想要获取id为s下的span需要用到另一种形式
         //先以id形式获取到他的父类
         //   var nn = document.getElementById('s');
         //   //   再使用上边的返回值调用他下边的span
         //   var ns = nn.getElementsByTagName('span');
         //   也可以使用链式调用形式
         var ns = document.getElementById('s').getElementsByTagName('span');
         console.log(ns);
         for (var i = 0; i < ns.length; i++) {
           console.log(ns[i]);
         }
         
   ```

3. 通过h5新增属性获取    有兼容性

   例：

   ```css
         // 根据类名获取元素集合
         document.getElementsByClassName('类名');
         // 返回指定选择器的第一个元素对象  需要加符号  如class是.a ，id是#b  直接写标签
         document.querySelector('选择器');
         // 返回指定选择器的所有元素对象集合  需要加符号  如class是.a ，id是#b
         document.querySelectorAll('选择器');
   ```

4. 获取body元素

   document.body;

5. 获取html元素

    document.documentElement;

### 事件基础

事件就是触发响应的机制   

##### 事件三要数

1. 事件源  事件被触发的对象    按钮

2. 事件类型   如何触发  什么事件  鼠标点击(onclick)

3. 事件处理程序   通过一个函数赋值方式  

   例：

   ```css
     <body>
       <button id="s">你好</button>
       <script>
         // 1. 事件源  事件被触发的对象    按钮
         var bt = document.getElementById('s');
         // 2. 事件类型   如何触发  什么事件  鼠标点击(onclick)
         // 3. 事件处理程序   通过一个函数赋值方式
         bt.onclick = function () {
           alert('你不好');
         };
       </script>
     </body>
   ```

##### 执行事件的步骤

1. 获取事件源

2. 注册事件(绑定事件)

3. 添加事件处理程序(采取函数赋值形式)

   例：

   ```css
        <!-- 改变元素内容 -->
   	<button>显示当前时间</button>
       <div>123</div>
       <p>789</p>
       <script>
         // 点击按钮   div里面文字发生变化
         // 获取元素
         var btn = document.querySelector('button');
         var div = document.querySelector('div');
         //   点击事件
         btn.onclick = function () {
           // 处理程序
           div.innerText = '2021.02.20';
         };
         //   也可以不写事件刷新直接改变标签中的文字
         var p = document.querySelector('p');
   	  //innerText不识别html标签只识别文本
   	  //innerhtml识别html标签也识别文本
         p.innerText = '12.21.31';
       </script>
   ```

   1. innerText不识别html标签只识别文本
   2. innerhtml识别html标签也识别文本

   ### 常见的鼠标事件

   ![image-20210406140000327](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210406140000327.png)
   
   ​			contextmenu          网页禁止右键菜单
   
   ​			selectstart                网页禁止选中文字
   
   ​			
   
   例：
   
   ```css
   //网页禁止右键菜单
   document.addEventListener('contextmenu',function(e){
       e.preventDefault();
   })
   //网页禁止选中文字
   document.addEventListener('selectstart',function(e){
       e.preventDefault();
   })
   ```
   
   
   
   ​			

##### 修改表单属性

禁用按钮    被禁用名.disabled = true;   true  被禁用    false   不被禁用

```css
  <body>
    <span>
      <input type="password" name="" id="" />
      <img src="./image/close.png" alt="" />
    </span>

    <script>
      var ipn = document.querySelector('input');
      var img = document.querySelector('img');
      var tre = true;

      img.onclick = function () {
        //   如果tre为true则点击后将密码显示
        //   并重新给tre赋值为false  让下次点击时再进行判断
        if (tre) {
          this.src = './image/open.png';
          ipn.type = 'text';
          tre = false;
        } else {
          this.src = './image/close.png';
          ipn.type = 'password';
          tre = true;
        }
      };
    </script>
  </body>
```

##### 样式属性操作

可以修改颜色、大小等css属性

使用.style.样式名  实现后实现为行内样式  

例： this.style.backgroundColor = 'red';

```css
    <style>
      div {
        width: 200px;
        height: 300px;
        border: 1px solig red;
        background-color: orchid;
      }
    </style>
  </head>
  <body>
    <div></div>
    <script>
      var div = document.querySelector('div');
      div.onclick = function () {
        this.style.backgroundColor = 'red';
        this.style.width = '100px';
        this.style.height = '50px';
      };
      //  pathIntellisense   插件
    </script>
  </body>
```

### 自定义属性

1. #### 获取自定义属性值getAttribute('属性');

2. #### 设置自定义属性setAttribute('属性'，属性值);

3. #### 删除自定义属性removeAttribute('属性');

### H5新增  自定义属性(有兼容性不常用)

![image-20210408162314191](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210408162314191.png)

### **节点**

1. ##### 找父节点使用     子节点名称.parentNode

2. ##### 找子节点        父节点名称. children  获取所有子元素节点  返回的是伪数组形式

   实际开发用：

   父节点名称. children 获取所有子元素

   父节点.children[ 0 - 父节点.children.length-1]

   children返回的是伪数组

   有兼容性：

   (1. ) 找第一个子元素节点    父节点名称.fristElementChild

   (2. )找最后子元素节点    父节点名称.lastElementChild

   

3. ##### 找兄弟节点   

   包含元素节点和文本节点：没有返回null    不常用

   (1. ) .nextSibling得到的是下一个兄弟节点   

   (2. ).previousSibling得到的是上一个兄弟节点

   有兼容性：  没有返回null

   (1. ) .nextElementSibling得到的是下一个兄弟元素节点   

   (2. ).previousElementSibling得到的是上一个兄弟元素节点

   也可以自己封装一个函数     了解就行

   ![image-20210409094758987](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210409094758987.png)

4. ##### 创建元素节点    document.createElement('')

5. ##### 添加节点    父节点.appendChild(子节点)将节点添加到父节点的子节点末尾，相当于伪元素

   

   例：

   ```css
   	//创建元素节点
   	var tr = document.createElement('tr');
       var td = document.createElement('td');
       var td1 =document.createElement('td');
   	//将节点添加到父节点的下边
       tbody.appendChild(tr);
       tr.appendChild(td);
   	tr.appendChild(td1);	
   	//将节点添加到父节点下的第某个节点
   	ul.insertBefore(li,ul.children[0])
   
   ```


6. ##### 删除节点

   父节点.removeChild(子节点)

   例：

   ```css
    var as = document.querySelectorAll('a');
    for (var i = 0; i < as.length; i++) {
    	as[i].onclick = function () { 			ul.removeChild(this.parentNode);
       };
     }
   ```

   

7. ##### 复制节点

   需要复制的节点.cloneNode() 

   例：

   ```css
       <button>影分身</button>
       <script>
         var but = document.querySelector('button');
         but.addEventListener('click', function () {
           //but.cloneNode()当小括号里不写东西时只复制标签不会复制标签内的内容
           //but.cloneNode(true)当小括号里为true时会复制该标签内的所有东西
           var tj = but.cloneNode(true);
           document.body.appendChild(tj);
         });
       </script>
   ```

   

# 新的事件监听   以后就用它addEventListener('类型',方法(){})

```css
      // 事件侦听注册事件
      //事件类型是字符串，必须要添加引号   不带on
      //同一个事件可以添加多个事件处理程序
      bt.addEventListener('click', function () {
        alert('fghjkl');
      });
      bt.addEventListener('click', function () {
        alert('aowihejndm');
      });
```

# 删除事件

```css
    //1. 传统删除事件
	var as = document.querySelectorAll('a');
 	as[1].onclick = function () { 						  			ul.removeChild(this.parentNode);
        as[1].onlick = null;
    };
 //2. 新的事件监听 删除需要将方法写在外边   里边的fn不用加小括号
      as[2].addEventListener('click',fn );
		function fn() {
            alert('aowihejndm');
            //使用removeEventListener()进行删除事件
            as[2].removeEventListener('click',fn );
      }
```

### DOM事件流

![image-20210409153740522](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210409153740522.png)

![image-20210409154946650](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210409154946650.png)

### 事件对象

例:

```css
	事件对象只有有了事件才会存在，它是系统给我们自动添加的不需要传递参数
	event是自己定义的常用e
as[1].onclick = function (event) { 						  			 alert(event);
    };

      as[2].addEventListener('click', function (e) {
        alert(e);
      });
```

![image-20210409161917013](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210409161917013.png)

### 事件对象属性方法

![image-20210409162836049](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210409162836049.png)

### 阻止默认行为e.preventDefault()

![image-20210409164603671](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210409164603671.png)

###  阻止冒泡  e.stopPropageation()

![image-20210410091839080](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210410091839080.png)

### 事件委托

事件委托的原理

不是每个子节点单独设置事件监听器，而是事件监听器设置在其父节点上，利用事件冒泡影响每一个子节点

```CSS
    <ul>
      <li>adsncx srndkfxcz1</li>
      <li>adsncx srndkfxcz2</li>
      <li>adsncx srndkfxcz3</li>
      <li>adsncx srndkfxcz4</li>
      <li>adsncx srndkfxcz5</li>
    </ul>
    <script>
      var ul = document.querySelector('ul');
      ul.addEventListener('click', function (e) {
      if (e.target != this) {
        var lis = this.children;
        for (var i = 0; i < lis.length; i++) {
          lis[i].style.backgroundColor = '';
        }
          //e.target   获取点击的东西
        e.target.style.backgroundColor = 'red';
        //this.style.backgroundColor = '';
          //获取点击的元素的值
        alert(e.target.innerHTML);
        }
        // console.log(e.target.innerHTML);
      });
    </script>
```

### 鼠标事件对象

![image-20210410102022179](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210410102022179.png)

### 键盘事件

![image-20210410111034403](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210410111034403.png)

例：

```css
      //   keyup不区分ascll码的字母大小值
	document.addEventListener('keyup', function (e) {
        // console.log('键盘抬起');
      });
      //   keydown能识别功能键
      //   keydown不区分ascll码的字母大小值
      document.addEventListener('keydown', function (e) {
        // console.log('键盘按下');
      });
      //   keypress不能识别功能键 比如ctrl  shift
      //   keypress区分ascll码的字母大小值
      document.addEventListener('keypress', function (e) {
        console.log('键盘按下');
        // 获取按下的哪个键  有兼容性
        console.log(e.key);
        // 获取axcll码值  可以根据keyCode的返回值来查看用户按下的是的哪个键
        console.log(e.keyCode);
      });
      //   执行顺序为  keydown--keypress-up-key
```

### ascll表

![image-20210410112101218](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210410112101218.png)

# BOM

### BOM的概述

BOM包含DOM

![image-20210410140832425](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210410140832425.png)

### BOM构成

![image-20210410141542127](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210410141542127.png)

常量不属于window  常量属于script脚本

load等页面加载完后才运行

DOMContentLoaded  等HTML加载完后就运行了

resize当页面可视窗口发生变化时触发

window.innerWidth    获取可视窗口宽度

window.innerHeight     获取可视窗口高度

例：

![image-20210410144345316](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210410144345316.png)

### 定时器(单次调用)window.setTimeout(调用函数,延时时间毫秒数);  

属于异步函数    最后执行

js运行机制 回调函数--->异步函数

可以省略window  延时时间毫秒数也可以省略，省略代表立即执行

只调用一次

```css
三种写法
1.
setTimeout(function(){
    console.log('俺老孙出来了')
},2000)

2.
function fn(){
    console.log('俺老孙出来了')
}
setTimeout(fn,2000)

3.不推荐
function fn(){
    console.log('俺老孙出来了')
}
setTimeout('fn()',2000);
```

### 清除单次定时器clearTimeout(定时器名称);

```css
 function fn(){
    console.log('俺老孙出来了')
}
var x = setTimeout(fn,2000);
clearTimeout(x);
```

### 定时器(循环调用)setInterval(调用函数,延时时间毫秒数),

每隔一个延时时间调用一次，反复调用，相当于循环

### 清除循环定时器clearInterval(定时器名称)

```css
 function fn(){
    console.log('俺老孙出来了')
}
var x = setInterval(fn,2000);
clearInterval(x);
```

### location对象

![image-20210412101337517](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210412101337517.png)

### location对象属性

![image-20210412101711498](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210412101711498.png)

直接log对象.属性就可以输出

也可以使用   对象.属性 = ''   给它重新赋值

### location的对象方法

![image-20210412113231760](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210412113231760.png)

### history对象方法

![image-20210412114454188](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210412114454188.png)

# PC端网页特效

### offset偏移量包含边框

![image-20210412140542807](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210412140542807.png)

### offset与style的区别

![image-20210412142249314](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210412142249314.png)

### client系列属性不包含边框

![image-20210412202943747](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210412202943747.png)

### 立即执行函数

立即执行函数的作用是：独立创建了一个作用域，避免命名冲突

立即执行不需要调用,多个立即执行函数之间需要用分号进行分割，尽量前后都加分号

```css
1.
(function fn(a,b,c){
    console.log(a+b+c);
})(1,2,3);
第二个小括号可以看作是调用函数，也可以传递实参
2.
(function fn(a,b,c){
     console.log(a+b+c);
}(1,2,3))
```

### e.persisted    如果这个页面是从缓存取出来的就返回true

### scroll系列属性

![image-20210412205051261](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210412205051261.png)

scroll滚动事件当滚动条滚动会触发

```css
语法：
div.addEventListener('scoll',function(){
    
})
```

元素被卷去头部用element.sctollTop获得

页面被卷去的头部：通过window.pageYOffset获得

页面被卷去的左侧  ：通过window.pageXOffset获得

![image-20210412210918301](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210412210918301.png)

### mouseenter和mouseover的区别

![image-20210412211059506](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210412211059506.png)

### 缓动动画公式： 

```css

 速度越来越慢
// 如果是正数Math.ceil()进行向上取整
 // 如果是负数Math.floor()进行向下取整
 // (目标值-现在的位置) / 10)
var yidong = (target - obj.offsetLeft) / 10;
 yidong = yidong > 0 ? Math.ceil(yidong) : Math.floor(yidong);
            

```

### 回调函数就是等函数执行后进行执行

```css
function st(a,b,cc){
    consol.log(a+b)
    cc();
}
st(1,2,function(){
    consol.log('da da da da')
})
```

### 手动调用事件

```css
div.addEventListener('click',function(){
     consol.log('da da da da');
});
// 直接使用.事件
div.click();
```



### 返回页面的某个位置window.scroll(x,y)

```css
window.scroll(x,y)
x，y坐标不跟单位
```

# 移动端特效

### 触屏事件

只有移动端才能使用

![image-20210415144649519](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210415144649519.png)

### 触摸列表

![image-20210415151424593](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210415151424593.png)

1. 如果侦听的是一个元素那么touches、changedTouches和tagerTouches的结果是一样的
2. 当手指离开元素是就没touches和tagerTouches的列表 。但是changedTouches会有
3. 实际开发经常使用**changedTouches**  返回的是数组形式
4. changedTouches需要用e.来使用
5. e.changedTouches[0]以对象的形式返回的是第一个手指的信息  如：坐标pageX,pageY

### 移动端拖动原理公式：

1.  盒子原来的距离+手指移动的距离
2. 手指移动的距离 = 手指滑动的位置-手指刚开始触摸的位置

### 拖动元素的步骤：

1. 触摸元素  tauchstart:获取手指初始坐标，同时获取盒子原来的距离
2. 移动手指  touchmove:计算手指的移动距离，并移动盒子

手指移动也会触发滚动屏幕所以要阻止默认的屏幕滚动e.preventDefault

### 监听过度是否完成事件  transitionend

### classList属性

```css
标签名.classList以数组的形式返回元素类名

添加类名：
标签名.classList.add('类名')

删除类名：
标签名.classList.remove('类名')

切换类名：
有这个类名就删除，没有就添加
标签名.classList.toggle('类名')
```

### 自动轮播

```css
	autoplay:{disableOnInteraction:false}
```

### 本地存储

![image-20210416151713185](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210416151713185.png)

#### 1. window.sessionStorage   关闭窗口数据清零

1. 关闭窗口数据清零
2. 在同一个窗口下数据可共享
3. 根据键值对的形式进行存储
4. 只能存储字符串
5. 最大存储5M

```css

//存储数据
sessionStorage.setItem('键',值);

//获取数据
sessionStorage.getItem('键');

//删除数据
sessionStorage.removeItem('键');

//清空所有数据
sessionStorage.clear();

```

#### 2. window.localStorage   关闭窗口数据不清零

1. 关闭窗口数据不清零
2. 在同一个窗口下数据可共享
3. 根据键值对的形式进行存储
4. 只能存储字符串
5. 最大存储20M

```CSS

//存储数据
localStorage.setItem('键',值);

//获取数据
localStorage.getItem('键');

//删除数据
localStorage.removeItem('键');

//清空所有数据
localStorage.clear();
```

#### 数组转成字符串

```css
JSON.stringify(数组名)
```

#### 字符串转数组

```css
JSON.parse()
```

