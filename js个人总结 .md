# JS

js内容可以写在<script></script>标签中   这种叫做内嵌式;

### 浏览器弹出框 

1. 警告框：alert("");
2. 可以输入的弹出框：prompt("提示语句");   接收的值为字符型，要想转为数字类型需要用Number(值);来进行转换
3. 控制台输出：console.log("");   快捷方式   log回车      使用F12才能看到

js内容可以也写在元素内部   这种叫行内式   不常用；

例：

```css
<input type="button" value="点击" onclick="alert('')">
```

js内容可以也写在外部引入进来  这种叫外部式   写项目常用

例：

```css
<script src="***.js"></script>
```

使用var定义的叫变量，使用const定义的叫常量。

​						常量必须给初始值，并且值不能更改



1. ### 使用   变量名.toString();     进行转化为字符串类型

2. ### 使用   String(变量名);          进行转化为字符串类型

3. ### 使用   Number(变量名);       进行转化为数字类型

4. ### 使用   parseInt(变量名);       进行转化为 整数 数字类型  并去掉整数后的小数位和非数字

5. ### 使用   parseFloat(变量名);    转化为 浮点数 数字类型   并去掉整数后的非数字

6. ### 使用    Boolean(变量);           转换为布尔类型    除了" "、0、NaN、null、undefined会转换为false，别的都会转换为ture;

7. ### 使用   typeof  变量名  ;         进行检测数据类型

8. ### 使用   continue;                     退出本次循环进行下次循环

![image-20210326153821450](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210326153821450.png)

isNaN()用来判断是否为非数字，

是数字返回false，不是数字返回true

### 转义符

![image-20210326161340884](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210326161340884.png)

### 检测字符串的长度length

​	变量名.length

例：

```css
var a ="你好！"
console.log(a.length);
```

双重循环：外循环循环一次 控制行数，内循环循环全部  控制列数；

### while循环

```css
	当条件表达式结果为true  则执行循环体   否则退出循环
while(条件表达式){
 	循环体   
    操作表达式;
}

例：
var i = 1;
while(i <= 10){
    alert("循环"+i+"次");
    i++;
}
```

### 三元表达式

例：

```css
语法结构：
条件表达式 ? 表达式1 : 表达式2;
```

![image-20210328200537907](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210328200537907.png)

### 短路运算

![image-20210329092732629](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210329092732629.png)

### switch和ifelse的区别

![image-20210329103818387](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210329103818387.png)

# 数组

​		数组是指一组数据的集合，其中的每个数据被称为元素，在数组中可以存放任意类型的元素。数组是一种将一组数据存储咋单个变量名下的方式。

### 创建数组：

1. 利用new创建数组

   ```css
   var 数组名 = new Array();//创建一个空数组
   ```

2. 利用数组字面量创建数组     常用 ！！！

   ```css
   使用字面量创建空数组
   var 数组名 = [];
   使用数组字面量创建带初始值的数据
   var 数组名 = ['喷火龙','皮卡丘','杰尼龟'];
   ```

数组中可以存放任意类型，例字符串，数字，布尔等。

### 获取数组

​	通过索引来将数组元素的下标获取到

​	例：	

```css
var a = ['喷火龙','皮卡丘','杰尼龟'];
格式   数组名[下标]    注意 下标从 0 开始
输出  喷火龙
alert(a[0]);
```

### 遍历数组

​	遍历就是把每个元素都输出出来

使用for循环来操作

### 查询数组的长度

​	数组名.length 来查询数组长度

数组长度是元素的个数  跟下标不同

例

```css
// 定义一个数组
        var zhou = ['星期一', '星期二', '星期三', '星期四', '星期五', '星期六', '星期日']

        // 根据下标来获取数组内的元素      下标从0开始
        console.log(zhou[6]);

        // 使用for循环将数组中的元素遍历出来
        for (i = 0; i < zhou.length; i++) {
            console.log("我是遍历出来的：" + zhou[i]);
        }
```

### 数组新增元素

1. 通过修改length长度新增数组元素

   length属性是可读写的

   例：

   ```css
   var a= ['你', '好', '世', '界',];
   //本来数组长度为4，现在修改为8个，多余的不赋值为undefined  未定义数据类型。
   a.length =8; 
   ```

2. 使用索引号来新增元素

   例:

   ```css
   var we = [];
   we[0]=18;
   ```

# 函数

定义：封装了一段可重复调用执行的代码块。

函数之间可以相互调用

### 1. 声明函数  function   关键字必须小写

```css
//语法结构(命名函数)
function 函数名(){
    函数体
}
例：
function mini(){
    函数体
}
2.函数表达式(匿名函数)
语法  
1.变量名不是函数名
2.相当于声明变量，只不过一个存的是值，一个存的是函数
var 变量名 = function(){
    函数体
}
```

### 2.调用函数

```css
两种声明方式都用这个进行调用
函数名();
```

### 形参与实参：

形参：声明函数小括号里的变量；

实参：调用函数小括号里的变量；

![image-20210328195556233](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210328195556233.png)

​	

### 函数返回值 return;

```css
//语法结构
function 函数名(变量名){
    return 变量名 ;
}
return 后面的代码不会执行
return 只能返回一个值 但是可以返回一个数组
return 可以结束循环，但是必须在函数中使用

```

![image-20210328200200123](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210328200200123.png)

![image-20210328201210424](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210328201210424.png)



### arguments的使用

arguments是一个伪数组，具有数组length属性，也是按照索引方式进行存储，

例：

```css
function nn() {
    //打印收到的值，结果为数组形式
        console.log(arguments);
    //打印这个数组的长度
        console.log(arguments.length);
    //以数组下标的形式来输出数组中的元素
        console.log(arguments[3]);
      }
      nn("胖子", "白沉香", "三哥", "小舞姐", "戴老大", "竹清", "小奥", "荣荣");
```

### 预解析

![image-20210401195511143](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210401195511143.png)



# 对象

万物皆对象，对象是一个具体的事物;

对象是由属性和方法组成的集合

1. 属性：事物的特征
2. 方法：事物的行为

### 创建对象的三种方法

1. #### 利用字面量创建对象

   ##### (1. )创建语法：

   ```css
   var 对象名 = {
       创建属性：
       属性名:属性值,
       uname: '张三丰',
       创建方法：
       sw: function(){
           console.log('我是函数')
       }
   }
   里面的属性或方法采取键值对的形式  键 属性名；值 属性值 可以是任何类型
   多个属性用逗号隔开
   方法后面跟一个匿名函数
   
   ```

   #####  (2. )使用对象的两种方法

   语法：

   ```css
   调用属性分两种
   1.
   对象名.属性名
   2.
   对象名['属性名'] ：  里面是变量不加引号，固定值要加引号
   调用里面的方法：
   对象名.方法名()
   ```

   

2. #### 利用 new Object 创建对象

   (1.)创建语法：

   ```css
   var 对象名 = new Object();
   添加属性：
   对象名.属性名 = 属性值;
   添加方法：
   对象名.方法名 = function (){}
   ```

   (2.)调用语法和上边一样

   

3. #### 利用构造函数创建对象

   前两种一次只能创造一个对象，构造函数何以创建多个对象

   构造函数就是把对象里边相同的属性和方法抽象出来封装在函数中

   (1. )创建语法：

   ```css
   	1. 构造函数首字母要大写
   	2. 构造函数不需要return，就可以返回结果
   	3. 调用函数必须使用new
   	4. 只要new 构造函数名  就会返回一个实例对象
   	5. 
   
   function 构造函数名(形参1,形参2,形参3,形参n){
       this指向实例对象
       
       this.属性 = 值;
       
       this.方法名 = function (形参a){}
       
       this.属性名 = 形参1;
       
       this.属性名 = 形参2;
       
       this.属性名 = 形参3;
       
       this.属性名 = 形参n;
   }
   
   调用构造函数  返回的是一个对象
   传参数并接受返回的实例对象：
   
   var 接收名 = new 构造函数名(实参1,实参2,实参3,实参n);
   调用构造函数：
   
   接收名.属性名
   接收名.方法名(实参a);
   ```

   ![image-20210402152714799](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210402152714799.png)

### 使用for   in  遍历对象

```css
先定义一个对象 
var star = {
        name: '皮卡丘',
        age: 18,
        jiao: '皮卡皮卡',
      };
	k 代表属性名
      for (var k in star) {
          输出对象里所有的属性名
        console.log(k);
          根据属性名输出对象里所有的属性值
        console.log(star[k]);
      }
```

# 内置对象

对象分为三种：自定义对象、内置对象、浏览器对象

![image-20210402153948545](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210402153948545.png)

### 绝对值math.abs(x);



### 随机数math.random();返回一个随机的小数 0-1之间，没有参数

### 随机整数，随机点名

```css
        //   得到两个数之间随机整数 并包括这两个数
        function getNu(min, max) {
          return Math.floor(Math.random() * (max - min + 1)) + min;
        }
        var sj = getNu(1, 100);
        console.log(sj);

        //随机人生
        var arr = [
          '乞丐',
          '贫民',
          '农民',
          '警察',
          '小偷',
          '企业家',
          '富人',
          '军人',
          '码农',
        ];

        var ren = arr[getNu(0, arr.length - 1)];
        console.log(`您的身份是：${ren}`);
```



### 格式化日期

```css
      //封装一个日期
      function riQi() {
        var time = new Date();
        var nian = ling(time.getFullYear());
        var yue = ling(time.getMonth() + 1);
        var ri = ling(time.getDate());
        var zhou = [
          '星期日',
          '星期一',
          '星期二',
          '星期三',
          '星期四',
          '星期五',
          '星期六',
        ];
        var xing = time.getDay();
        return `现在是${nian}年${yue}月${ri}日 ${zhou[xing]}`;
      }
      //封装一个补零函数
      function ling(t) {
        return t < 10 ? '0' + t : t;
      }

      //调用日期函数
      var x = riQi();
      console.log(x);
```

![image-20210406201356053](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210406201356053.png)



### 格式化时分秒

```css
      //封装一个补零函数
      function ling(t) {
        return t < 10 ? '0' + t : t;
      }

      //封装一个时间函数
      function shiJian() {
        var shi = new Date();
        var xiao = ling(shi.getHours());
        var fen = ling(shi.getMinutes());
        var miao = ling(shi.getSeconds());
        return `${xiao}:${fen}:${miao}`;
      }
      //调用时间函数
      var s = shiJian();
      console.log(s);

```

### 获取毫秒数

```css
      //获取总的毫秒数 的四种方法
      var h = new Date();
      console.log(h.valueOf());
      console.log(h.getTime());
      //有兼容性
      console.log(Date.now());
      var hs = +new Date();
      console.log(hs);
```

### 倒计时案例

```css
      //倒计时
      function dao() {
        var shu = prompt('请输入您想结束的时间  例：2021-04-08 09:20:30');
        var ji = new Date(shu);
          //输入的时间转换为时间戳减去现在的时间戳就得出剩余的时间戳
            //因为结果是毫秒数  毫秒要转换为秒数
        var zong = (ji.valueOf() - Date.now()) / 1000; 
          //转换时间戳的公式
        var d = ling(parseInt(zong / 60 / 60 / 24)); //计算天数
        var h = ling(parseInt((zong / 60 / 60) % 24)); //计算小时
        var m = ling(parseInt((zong / 60) % 60)); //计算分钟
        var s = ling(parseInt(zong % 60)); //计算秒
        return `现在还剩${d}天${h}小时${m}分${s}秒`;
      }

      console.log(dao());
```

### 检测是否为数组

1. instanceof  返回结果为true 或false

   数组即是数组也是对象

2. Array.isArray()  返回结果为true 或false  

  例：

   ```css
 //翻转数组案例进行判断
   function ni(hao) {
           // if (hao instanceof Array) {
           if (Array.isArray(hao)) {
             var ya = [];
             for (var i = hao.length - 1; i >= 0; i--) {
               ya[ya.length] = hao[i];
             }
             return ya;
           } else {
             return '抱歉您输入的不是数组形式不能进行翻转';
           }
         }
         var gu = ni([2, 3, 5, 85, 84, 66, 77, 11]);
         console.log(gu);
         var gu1 = ni(2, 3, 5, 85, 84, 66, 77, 110);
     		//结果返回else内的内容
         console.log(gu1);
   ```

### 添加数组元素

1. push();在末尾添加一个或多个元素
2. 将元素添加到数组的头部 unshift();

例：

```css
var a = [3,2,1,3];
a.push(5);
添加多个元素
a.push(7,8,4);
返回当前数组的最新长度
a.unshift("为","是");
并返回数组的最新长度
```

### 删除数组元素

1. pop();删除数组最后一位元素
2. 删除数组中第一个元素  shift();

例：

```css
console.log(a.pop());
返回删除的元素
a.shift();
返回删除的元素
```

### 翻转数组的方法直接使用    数组名.reverse();

### 数组排序的方法直接使用    

数组名.sort();

例：

```css
var r = [7,8,9,10,5,4];
r.sort(function(a,b){
    return a-b;//升序排列
    return b-a;//降序排列
})
```

### 返回数组元素索引

1. 使用indexof(数组元素)

2. 使用lastIndexof(数组元素)

   例：

   ```css
   var a = ["hu","yi","fei","ya","hu","yao"];
   他会返回1
   console.log(a.indexof("yi"))
   他会返回0
   console.log(a.indexof("hu"))
   如果有两个相同的元素会返回第一个的索引
   如果找不到就返回-1
   从前往后查
   
   从后往前查
   他会返回4
   console.log(a.lastIndexof("hu"))
   ```

   ### 数组去重

   例：

   ![image-20210405150936726](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210405150936726.png)

### 将数组转换为字符串

![image-20210405151425634](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210405151425634.png)

### 删除数组的某一个元素

![image-20210405152259134](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210405152259134.png)

### 根据字符串返回位置

![image-20210405154008238](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210405154008238.png)

### 根据元素查找元素出现的位置及次数

![image-20210405154604165](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210405154604165.png)

### 根据位置返回字符

1. charAt(index);

2. charCodeAt(index);

3. str[index]  H5新增有兼容性

   例：

   ![image-20210405160813122](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210405160813122.png)

### 字符串操作

划红线的为重点记忆

![image-20210405163314867](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210405163314867.png)

![image-20210405163458560](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210405163458560.png)

![image-20210405164230876](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210405164230876.png)

### 统计字符串中字符出现的次数

![image-20210405182641769](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210405182641769.png)

![image-20210405182748041](C:\Users\苏永波\AppData\Roaming\Typora\typora-user-images\image-20210405182748041.png)